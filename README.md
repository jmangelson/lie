# README #

### Lie ###

Lie is a library that enables easy uncertainty characterization and propagation on elements of the
Special Euclidean group and the Special Orthogonal group in two and three dimensions.

### How to build the code? ###

Required third party libs:

* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)
* [python-2.7](https://packages.ubuntu.com/search?keywords=python2.7) (Needed only for evaluation plotting.)

This software is constructed according to the [Pods software policies and templates](http://sourceforge.net/projects/pods).

To install:

**Local:** `make`. The project will search 4 levels up the tree for
a `build/` directory. If one is not found it will be installed to
`build/` in the local directory.

**System-wide:** `make BUILD_PREFIX=<build_location>` as root.

### How to use the library? ###

An example is given in `src/lie_examples/`.

### Unit Testing
This project uses [GTest](https://github.com/google/googletest) for unit testing and will automatically download it locally when
you build.

Individual test binaries can be found in `test/` in the build directory.
`make test` will run all tests.

### License and Attribution

You can freely use this software as long as you cite both our work and that
of Barfoot et. al.  and follow the notices in LICENSE.

### Questions or Contact ###

If you use this code in your research, we would love hear about it. 
In you have any questions, you can contact us via the following email:

Joshua Mangelson
mangelso@umich.edu

#ifndef __LIE_LIE_HPP
#define __LIE_LIE_HPP

#undef NDEBUG
#include <memory>
#include <type_traits>
#include <vector>
#include <tuple>
#include <iostream>
#include <typeinfo>

#include <Eigen/Core>

#include "tuple_get_runtime.hpp"

namespace Lie
{

//////////////////////////////////////////////////////////////
// Base Class and CRTP Template Classes for Lie Group Types
//////////////////////////////////////////////////////////////
class LieGroupBase
{
 private:
  int dim_;
 public:
  LieGroupBase(int dim) : dim_(dim) {}
  virtual ~LieGroupBase() {};
  
  int algebra_dim() const { return dim_; }
};

// LieGroup CRTP Base Class Template
//
// Patterned after Lie group class in gtsam
// gtsam 4.0 gtsam / gtsam / base / Lie.h
//
template <class Class, int N>
class LieGroup : public LieGroupBase
{
  static_assert(N > 0,
                "Dimension N must be positive");
  static_assert(N != Eigen::Dynamic,
                "LieGroup not yet specialized for dynamically sized types.");

 public:
  enum { dimension = N };
  typedef Eigen::Matrix<double, N, 1> TangentVector;

 public:
  LieGroup() :
      LieGroupBase(N)
  {}

  //  int algebra_dim() {return N;}

  virtual ~LieGroup() {};

  const Class & derived() const {
    return static_cast<const Class&>(*this);
  }

  Class inverse() const {
    return derived().inverse();
  }

  Class compose(const Class& g) const {
    return derived() * g;
  }

  template<typename UncertainElementRefT>
  UncertainElementRefT compose(const UncertainElementRefT& g) const;

  Class between(const Class& g) const {
    return derived().inverse() * g;
  }

  // template<typename UncertainElementRefT>
  //  UncertainElementT between(const UncertainElementRefT& g) const;
  
  static Class Exp(const TangentVector& v) {
    return Class::Exp(v);
  }

  static TangentVector Log(const Class& g) {
    return Class::Log(g);
  }

  static Eigen::Matrix<double, N, N> Ad(const Class& g) {
    return Class::Ad(g);
  }
};


////////////////////////////////////////////////////////////////////////////////////
// Class for Tracking the Estimated Mean and Cov of a Group of Uncertain Elements
////////////////////////////////////////////////////////////////////////////////////
template<bool...> struct check;
template<bool... b> struct check<false, b...>: std::false_type {};
template<bool... b> struct check<true, b...>: check<b...> {};
template<> struct check<>: std::true_type {};

template<typename... T>
class UncertainState
{
  static_assert(check<std::is_base_of<LieGroupBase, T>::value...>::value,
                "Template parameters for UncertainState must have LieGroupBase as base.");
  
 private:
  std::tuple<T ...> mean_;
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> cov_;
  std::vector<int> offset_;
  std::vector<int> algebra_dims_;

  int total_dim;

  template<std::size_t I = 0, typename... Tp>
  inline typename std::enable_if<I == sizeof...(Tp), void>::type
  add_group_elements(std::tuple<Tp...>& t)
  { }

  template<std::size_t I = 0, typename... Tp>
  inline typename std::enable_if<I < sizeof...(Tp), void>::type
  add_group_elements(std::tuple<Tp...>& t)
  {
    LieGroupBase g = std::get<I>(t); // Passed in types must be derived from LieGroupBase
    this->offset_.push_back(this->total_dim);
    this->total_dim += g.algebra_dim();
    this->algebra_dims_.push_back(g.algebra_dim());

    add_group_elements<I + 1, Tp...>(t);
  }
  
 public:
  UncertainState(std::tuple<T...> mean,
                 Eigen::MatrixXd& cov) :
                 
      mean_(mean),
      cov_(cov),
      offset_(),
      algebra_dims_(),
      total_dim(0)
  {
    add_group_elements(mean);
  
    assert(cov.rows() == this->total_dim);
    assert(cov.cols() == this->total_dim);
  }
  
  UncertainState(std::tuple<T...> mean,
                 Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& cov) :
                 
      mean_(mean),
      cov_(cov),
      offset_(),
      algebra_dims_(),
      total_dim(0)
  {
    add_group_elements(mean);

    assert(cov.rows() == this->total_dim);
    assert(cov.cols() == this->total_dim);
  }
  
  ~UncertainState(){};

  const LieGroupBase& mu(int index) {return get_runtime<LieGroupBase>(this->mean_, index);}
  //const LieGroupBase& mu(int index) {return runtime_get(this->mean_, index);}  

  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>  
  marginal_cov(int index) const 
  {
    assert(index < this->offset_.size() && "Invalid indices passed into marginal covariance");
    assert(index >= 0 && "Invalid indices passed into marginal covariance");
    
    int start = this->offset_[index];
    int dim = this->algebra_dims_[index];
    return this->cov_.block(start, start, dim, dim);
  }
  
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>  
  marginal_cov(std::vector<int> indices) const
  {
    int total_dim = 0;
    for(int i : indices)
    {
      assert(i < this->offset_.size() && "Invalid indices passed into marginal covariance");
      assert(i >= 0 && "Invalid indices passed into marginal covariance");
      total_dim += this->algebra_dims_[i];
    }

    Eigen::MatrixXd marginal(total_dim, total_dim);

    int i_local_offset = 0;
    for(int i : indices)
    {
      int i_global_offset = this->offset_[i];
      int i_dim = this->algebra_dims_[i];


      int j_local_offset = 0;
      for(int j : indices)
      {
        int j_global_offset = this->offset_[j];
        int j_dim = this->algebra_dims_[j];

        marginal.block(i_local_offset, j_local_offset, i_dim, j_dim) =
            this->cov_.block(i_global_offset, j_global_offset, i_dim, j_dim);

        j_local_offset += j_dim;
      }

      i_local_offset += i_dim;
    }

    return marginal;
  }
  
  int get_num_objects() {return offset_.size();}
};

/////////////////////////////////////////////////////
// Base Class for a Single Uncertain Group Element
/////////////////////////////////////////////////////
template<typename LieGroupT,
         typename StateT>
class UncertainGroupElementRef
{
 public:
  typedef LieGroupT LieGroupType;
  typedef StateT StateType;
  
 private:
  std::shared_ptr<StateT> source_state_;
  int index_in_state_;

 public:
  UncertainGroupElementRef(std::shared_ptr<StateT> state,
                           int index) :
      source_state_(state),
      index_in_state_(index)
  {
    assert(index >= 0 && index < state->get_num_objects());
  }
  UncertainGroupElementRef(const UncertainGroupElementRef<LieGroupT, StateT>& ref):
      source_state_(ref.get_raw_underlying_state()),
      index_in_state_(ref.get_index_in_state())
  {}
  
  ~UncertainGroupElementRef(){};

  // Get mean of uncertain element
  const LieGroupT& mu() const
  {
    return dynamic_cast<const LieGroupT&>(this->source_state_->mu(
        this->index_in_state_));
  }

  // Get covariance of uncertain element  
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
  marginal_cov() const
  {
    return this->source_state_->marginal_cov(
        this->index_in_state_);
  }

  // Get raw underlying state
  const std::shared_ptr<StateT>
  get_raw_underlying_state() const
  {
    return source_state_;
  }

  int
  get_index_in_state() const
  {
    return index_in_state_;
  }

  template<typename UncertainElementRefT>
  bool has_same_underlying_state_as(const UncertainElementRefT& rhs) const;
  
  // bool has_same_underlying_state_as(const   UncertainGroupElementRef<LieGroupT, StateT>& rhs) const;
  
  template<typename UncertainElementRefT>
  bool is_independent_of(const UncertainElementRefT& rhs, Eigen::MatrixXd& joint_marg) const;

  template<typename UncertainElementRefT>
  bool is_independent_of(const UncertainElementRefT& rhs) const;

  // Compose with other uncertain and known lie group elements of the same type
  UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> > 
  //  UncertainGroupElementRef<LieGroupT, StateT>
  compose(const UncertainGroupElementRef<LieGroupT, StateT>& rhs) const;

  //  UncertainGroupElementRef<LieGroupT, StateT>
  UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> >   
  compose(const LieGroupT& rhs) const;

  UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> > 
  inverse() const;  
};

template< typename T > 
struct is_uncertain_element_ref{ 
  static const bool value = false;
};

template <>
template<typename LieGroupT,
         typename StateT>
struct is_uncertain_element_ref<UncertainGroupElementRef<LieGroupT, StateT> >{ 
  static const bool value = true;
};

template<typename LieGroupT,
         typename StateT>
template<typename UncertainElementRefT>
bool UncertainGroupElementRef<LieGroupT, StateT>::has_same_underlying_state_as(
    const UncertainElementRefT& rhs) const
{
  static_assert(is_uncertain_element_ref<UncertainElementRefT>::value,
                "Argument to is_independent_of is not an uncertain lie group element.");  
  
  return
      static_cast<void*>(this->get_raw_underlying_state().get()) ==
      static_cast<void*>(rhs.get_raw_underlying_state().get());
}

template<typename LieGroupT,
         typename StateT>
template<typename UncertainElementRefT>
bool UncertainGroupElementRef<LieGroupT, StateT>::is_independent_of(
    const UncertainElementRefT& rhs,
    Eigen::MatrixXd& joint_marg) const
{
  static_assert(is_uncertain_element_ref<UncertainElementRefT>::value,
                "Argument to is_independent_of is not an uncertain lie group element.");  

  if(!this->has_same_underlying_state_as(rhs))
  {
    return true;
  }

  joint_marg = this->get_raw_underlying_state()->marginal_cov({this->get_index_in_state(), rhs.get_index_in_state()});

  int dim1 = this->mu().algebra_dim();
  int dim2 = rhs.mu().algebra_dim();
    
  auto cross_cov = joint_marg.block(0, dim1, dim1, dim2);

  return cross_cov.isZero(1e-10);
}

template<typename LieGroupT,
         typename StateT>
template<typename UncertainElementRefT>
bool UncertainGroupElementRef<LieGroupT, StateT>::is_independent_of(
    const UncertainElementRefT& rhs) const
{
  Eigen::MatrixXd joint_marg;
  return this->is_independent_of(rhs, joint_marg);
}





//////////////////////////////////////////////////////////////////////////////
// Factories for UncertainState/UncertainGroupElementRef Classes
//////////////////////////////////////////////////////////////////////////////

// Template boilerplate functions
// User never calls these functions
template<typename LieGroupT, typename StateT>
UncertainGroupElementRef<LieGroupT, StateT>
make_uncertain_element_ref(int index, std::shared_ptr<StateT> state)
{
  return UncertainGroupElementRef<LieGroupT, StateT>(state, index);
}

template <int... I> struct index_sequence {};
template <int N, int... I>
struct make_index_sequence : make_index_sequence<N-1,N-1,I...> {};
template <int... I>
struct make_index_sequence<0, I...> : index_sequence<I...> {};

template<typename StateT, typename... Values, int... I>
auto make_tuple_of_refs(index_sequence<I...>, std::shared_ptr<StateT> state) ->
    decltype(std::make_tuple(make_uncertain_element_ref<Values, StateT>(I, state)...)) {
  return std::make_tuple(make_uncertain_element_ref<Values, StateT>(I, state)...);
}

template<typename StateT, typename... Values>
auto make_tuple_of_refs(std::shared_ptr<StateT> state) ->
    decltype(make_tuple_of_refs<StateT, Values...>(make_index_sequence<sizeof...(Values)>(), state)) {
  return make_tuple_of_refs<StateT, Values...>(make_index_sequence<sizeof...(Values)>(), state);
}

//////////////////////////////////////////////////////////
// This factory creates an arbitrary state from
// a tuple of mean group elements and a covariance matrix
template<typename... T>
std::tuple< UncertainGroupElementRef<T, UncertainState<T...> >... >
make_uncertain_state(std::tuple<T...> mean, Eigen::MatrixXd cov)
{
  static_assert(check<std::is_base_of<LieGroupBase, T>::value...>::value,
                "Template parameters for tuple passed into make_uncertain_state must have LieGroupBase as base.");
  
  typedef UncertainState<T...> StateT;
  typedef std::tuple< UncertainGroupElementRef<T, UncertainState<T...> >... > ReturnT;
  
  auto uncertain_state = std::make_shared<StateT>(mean, cov);

  ReturnT tuple_of_refs = make_tuple_of_refs<StateT, T...>(uncertain_state);

  return tuple_of_refs;
}

/////////////////////////////////////////////////////////////////////////
// This factory creates a single independent uncertain lie group element 
// of an arbitrary lie group type from a mean and covariance
template<typename LieGroupT>
UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> >
make_uncertain_state(LieGroupT mean, Eigen::MatrixXd cov)
{
  static_assert(std::is_base_of<LieGroupBase, LieGroupT>::value,
                "LieGroup element type LieGroupT passed into make_uncertain_state must have LieGroupBase as base.");
  
  typedef UncertainState<LieGroupT> StateT;
  typedef UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> > ReturnT;

  auto uncertain_state = std::make_shared<StateT>(std::make_tuple(mean), cov);

  ReturnT uncertain_element_ref(uncertain_state, 0);

  return uncertain_element_ref;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Functions For Composing Two Uncertain/Known LieGroup Elements (of the same lie group type)
///////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// Both known is implemented in the Lie Group Class
template<typename Derived>
typename std::enable_if<std::is_base_of<LieGroupBase, Derived>::value, Derived>::type
compose(const Derived& lhs, const Derived& rhs)
{
  auto res = lhs.compose(rhs);
  return res;
}


/////////////////////////////////////
// Both Lie Group Elements Uncertain
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
compose(const UncertainElementRefT& lhs, const UncertainElementRefT& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;


  LieGroupT mu_lhs = lhs.mu();
  LieGroupT mu_rhs = rhs.mu();
  
  
  LieGroupT mu = compose(mu_lhs, mu_rhs);
  
  int lie_algebra_dim = mu.algebra_dim();

  // Check if jointly correlated or independent

  Eigen::MatrixXd cov(lie_algebra_dim, lie_algebra_dim);

  Eigen::MatrixXd joint_marginal;
  if(lhs.is_independent_of(rhs, joint_marginal))
  {
    // If independent
    Eigen::MatrixXd lhs_marg = lhs.marginal_cov();
    Eigen::MatrixXd rhs_marg = rhs.marginal_cov();    

    auto Ad_LHS = LieGroupT::Ad(mu_lhs);
    
    // Upto 1st Order
    cov = lhs_marg + Ad_LHS * rhs_marg * Ad_LHS.transpose();
  }
  else
  {
    // If jointly correlated
    Eigen::MatrixXd lhs_marg = lhs.marginal_cov();
    Eigen::MatrixXd rhs_marg = rhs.marginal_cov();
    Eigen::MatrixXd Sigma_lr = joint_marginal.block(0, lie_algebra_dim, lie_algebra_dim, lie_algebra_dim);


    auto Ad_LHS = LieGroupT::Ad(mu_lhs);

    // Upto 1st Order
    cov = lhs_marg + Ad_LHS * rhs_marg * Ad_LHS.transpose() + Sigma_lr * Ad_LHS.transpose() + Ad_LHS*Sigma_lr.transpose();
  }

  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the UncertainGroupElementRef class, such that
// // composition of the following form is possible:
// // T.compose(g), where T and g are both unknown
// template<typename LieGroupT, typename StateT>
// UncertainGroupElementRef<LieGroupT, StateT>
// UncertainGroupElementRef<LieGroupT, StateT>::compose(
//     const UncertainGroupElementRef<LieGroupT, StateT>& rhs) const
// {
//   return Lie::compose(*this, rhs);
// }

////////////////////////////////////
// LHS is known, RHS is uncertain
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
compose(const typename UncertainElementRefT::LieGroupType& lhs, const UncertainElementRefT& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;
  
  LieGroupT mu = compose(lhs, rhs.mu());
  
  int lie_algebra_dim = mu.algebra_dim();
  auto Ad_LHS = LieGroupT::Ad(lhs);
  
  Eigen::MatrixXd cov = Ad_LHS * rhs.marginal_cov() * Ad_LHS.transpose();
  
  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the LieGroup class, such that
// // composition of the following form is possible:
// // T.compose(g), where T is known and g is unknown
// template<class Class, int N>
// template<typename UncertainElementRefT>
// UncertainElementRefT LieGroup<Class, N>::compose(const UncertainElementRefT& g) const
// {
//   static_assert(is_uncertain_element_ref<UncertainElementRefT>::value,
//                 "Argument to compose is not an uncertain lie group element.");
//   static_assert(std::is_same<LieGroup<Class, N>, typename UncertainElementRefT::LieGroupType>::value,
//                 "Argument to compose is uncertain lie group element of non-matching lie group.");

//   return Lie::compose(*this, g);
// }


///////////////////////////////////
// LHS is uncertain, RHS is known
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
compose(const UncertainElementRefT& lhs, const typename UncertainElementRefT::LieGroupType& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;
  
  LieGroupT mu = compose(lhs.mu(), rhs);
  
  int lie_algebra_dim = mu.algebra_dim();
  Eigen::MatrixXd cov = lhs.marginal_cov();
  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the UncertainGroupElementRef class, such that
// // composition of the following form is possible:
// // T.compose(g), where T is unknown and g is known
// template<typename LieGroupT, typename StateT>
// UncertainGroupElementRef<LieGroupT, StateT>
// UncertainGroupElementRef<LieGroupT, StateT>::compose(const LieGroupT& rhs) const
// {
//   return Lie::compose(*this, rhs);
// }


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Function For Inverting an Uncertain LieGroup Element
///////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename LieGroupT, typename StateT>
UncertainGroupElementRef<LieGroupT, UncertainState<LieGroupT> > 
UncertainGroupElementRef<LieGroupT, StateT>::inverse() const
{
  LieGroupT new_mean = this->mu().inverse();
  //  LieGroupT inverse_twice = mean.inverse() * mean.inverse();

  auto Ad_Inv = LieGroupT::Ad(new_mean);
  
  Eigen::MatrixXd cov = Ad_Inv * this->marginal_cov() * Ad_Inv.transpose(); 
  //Eigen::MatrixXd cov = Ad_Inv.transpose() * this->marginal_cov() * Ad_Inv; 
  
  auto return_value = Lie::make_uncertain_state(new_mean, cov);
  return return_value;
  
  //  return Lie::compose(inverse_twice, *this);
  //  auto I = Lie::compose(mean.inverse(), *this);
  //  return I; //Lie::compose(mean.inverse(), I);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Functions For Finding the Transformation Between Two Uncertain/Known LieGroup Elements
// (of the same lie group type)
///////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////
// Both known is implemented in the Lie Group Class

template<typename Derived>
typename std::enable_if<std::is_base_of<LieGroupBase, Derived>::value, Derived>::type
between(const Derived& lhs, const Derived& rhs)
{
  return lhs.between(rhs);
}

/////////////////////////////////////
// Both Lie Group Elements Uncertain
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
between(const UncertainElementRefT& lhs, const UncertainElementRefT& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;


  LieGroupT mu_lhs = lhs.mu();
  LieGroupT mu_rhs = rhs.mu();
  

  LieGroupT mu_lhs_inverse = mu_lhs.inverse();
  LieGroupT mu = between(mu_lhs, mu_rhs);
  
  int lie_algebra_dim = mu.algebra_dim();

  // Check if jointly correlated or independent

  Eigen::MatrixXd cov(lie_algebra_dim, lie_algebra_dim);

  Eigen::MatrixXd joint_marginal;
  if(lhs.is_independent_of(rhs, joint_marginal))
  {
    // If independent
    Eigen::MatrixXd lhs_marg = lhs.marginal_cov();
    Eigen::MatrixXd rhs_marg = rhs.marginal_cov();    

    auto Ad_LHS_Inv = LieGroupT::Ad(mu_lhs_inverse);
    
    // Upto 1st Order
    cov =
        Ad_LHS_Inv * lhs_marg * Ad_LHS_Inv.transpose() +
        Ad_LHS_Inv * rhs_marg * Ad_LHS_Inv.transpose();
  }
  else
  {
    // If jointly correlated
    Eigen::MatrixXd lhs_marg = lhs.marginal_cov();
    Eigen::MatrixXd rhs_marg = rhs.marginal_cov();
    Eigen::MatrixXd Sigma_lr = joint_marginal.block(0, lie_algebra_dim, lie_algebra_dim, lie_algebra_dim);


    auto Ad_LHS_Inv = LieGroupT::Ad(mu_lhs_inverse);

    // Upto 1st Order
    cov =
        Ad_LHS_Inv * lhs_marg * Ad_LHS_Inv.transpose() +
        Ad_LHS_Inv * rhs_marg * Ad_LHS_Inv.transpose() -
        Ad_LHS_Inv * Sigma_lr * Ad_LHS_Inv.transpose() -
        Ad_LHS_Inv * Sigma_lr.transpose() * Ad_LHS_Inv.transpose();
  }

  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the UncertainGroupElementRef class, such that
// // relative pose calculation of the following form is possible:
// // T.between(g), where T and g are both unknown
// template<typename LieGroupT, typename StateT>
// UncertainGroupElementRef<LieGroupT, StateT>
// UncertainGroupElementRef<LieGroupT, StateT>::between(
//     const UncertainGroupElementRef<LieGroupT, StateT>& rhs) const
// {
//   return Lie::between(*this, rhs);
// }

////////////////////////////////////
// LHS is known, RHS is uncertain
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
between(const typename UncertainElementRefT::LieGroupType& lhs, const UncertainElementRefT& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;
  
  LieGroupT mu = between(lhs, rhs.mu());
  
  //  int lie_algebra_dim = mu.algebra_dim();
  auto Ad_LHS_Inv = LieGroupT::Ad(lhs.inverse());
  
  Eigen::MatrixXd cov = Ad_LHS_Inv * rhs.marginal_cov() * Ad_LHS_Inv.transpose();
  
  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the LieGroup class, such that
// // relative pose calculation of the following form is possible:
// // T.between(g), where T is known and g is unknown
// template<class Class, int N>
// template<typename UncertainElementRefT>
// UncertainElementRefT LieGroup<Class, N>::between(const UncertainElementRefT& g) const
// {
//   static_assert(is_uncertain_element_ref<UncertainElementRefT>::value,
//                 "Argument to compose is not an uncertain lie group element.");
//   static_assert(std::is_same<LieGroup<Class, N>, typename UncertainElementRefT::LieGroupType>::value,
//                 "Argument to compose is uncertain lie group element of non-matching lie group.");

//   return Lie::between(*this, g);
// }


///////////////////////////////////
// LHS is uncertain, RHS is known
template<typename UncertainElementRefT>
UncertainGroupElementRef<typename UncertainElementRefT::LieGroupType,
                         UncertainState<typename UncertainElementRefT::LieGroupType> >
between(const UncertainElementRefT& lhs, const typename UncertainElementRefT::LieGroupType& rhs)
{
  typedef UncertainState<typename UncertainElementRefT::LieGroupType> StateT;
  typedef typename UncertainElementRefT::LieGroupType LieGroupT;
  
  LieGroupT mu = between(lhs.mu(), rhs);
  
  //  int lie_algebra_dim = mu.algebra_dim();
  auto Ad_LHS_Inv = LieGroupT::Ad(lhs.inverse());  
  Eigen::MatrixXd cov = Ad_LHS_Inv * lhs.marginal_cov() * Ad_LHS_Inv.transpose();
  auto state = std::make_shared< StateT > ( std::make_tuple(mu), cov );

  UncertainGroupElementRef< LieGroupT, StateT > result(state, 0);

  return result;
}

// // Specializes the template in the UncertainGroupElementRef class, such that
// // relative pose calculation of the following form is possible:
// // T.between(g), where T is unknown and g is known
// template<typename LieGroupT, typename StateT>
// UncertainGroupElementRef<LieGroupT, StateT>
// UncertainGroupElementRef<LieGroupT, StateT>::between(const LieGroupT& rhs) const
// {
//   return Lie::between(*this, rhs);
// }




}; // namespace Lie
#endif //__LIE_LIE_HPP

#ifndef __INDEX_SEQUENCE_HPP
#define __INDEX_SEQUENCE_HPP

// Define Index Sequence
//http://www.pdimov.com/cpp2/simple_cxx11_metaprogramming.html
template<class T, T... Ints> struct integer_sequence
{
};

template<class S> struct next_integer_sequence;

template<class T, T... Ints> struct next_integer_sequence<integer_sequence<T, Ints...>>
{
    using type = integer_sequence<T, Ints..., sizeof...(Ints)>;
};

template<class T, T I, T N> struct make_int_seq_impl;

template<class T, T N>
    using make_integer_sequence = typename make_int_seq_impl<T, 0, N>::type;

template<class T, T I, T N> struct make_int_seq_impl
{
    using type = typename next_integer_sequence<
        typename make_int_seq_impl<T, I+1, N>::type>::type;
};

template<class T, T N> struct make_int_seq_impl<T, N, N>
{
    using type = integer_sequence<T>;
};

template<std::size_t... Ints>
    using index_sequence = integer_sequence<std::size_t, Ints...>;

template<std::size_t N>
    using make_index_sequence = make_integer_sequence<std::size_t, N>;




// template<class S> struct mp_from_sequence_impl;

// template<template<class T, T... I> class S, class U, U... J>
//     struct mp_from_sequence_impl<S<U, J...>>
// {
//     using type = mp_list<std::integral_constant<U, J>...>;
// };

// template<class S> using mp_from_sequence = typename mp_from_sequence_impl<S>::type;


// //https://stackoverflow.com/questions/17424477/implementation-c14-make-integer-sequence
// template <size_t... Ints>
// struct index_sequence
// {
//   using type = index_sequence;
//   using value_type = size_t;
//   static constexpr std::size_t size() noexcept { return sizeof...(Ints); }
// };

// // --------------------------------------------------------------

// template <class Sequence1, class Sequence2>
// struct _merge_and_renumber;

// template <size_t... I1, size_t... I2>
// struct _merge_and_renumber<index_sequence<I1...>, index_sequence<I2...>>
//     : index_sequence<I1..., (sizeof...(I1)+I2)...>
// { };

// // --------------------------------------------------------------

// template <size_t N>
// struct make_index_sequence
//     : _merge_and_renumber<typename make_index_sequence<N/2>::type,
//                           typename make_index_sequence<N - N/2>::type>
// { };

// template<> struct make_index_sequence<0> : index_sequence<> { };
// template<> struct make_index_sequence<1> : index_sequence<0> { };

#endif //__INDEX_SEQUENCE_HPP

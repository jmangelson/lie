#ifndef __LIE_COORD_CHANGE_HPP
#define __LIE_COORD_CHANGE_HPP

#undef NDEBUG
#include <functional>

#include "index_sequence.hpp"
#include "tuple_get_runtime2.hpp"

#include "lie.hpp"
#include "ut.hpp"

namespace Lie
{

///////////////////////////////////////////////////////////
// Apply Logarithm Function to a tuple of group elements
//
template<typename... T>
Eigen::VectorXd log_tuple(std::tuple <T...> objects)
{
  int num_objects = std::tuple_size<decltype(objects)>::value;
  int tangent_vector_length = 0;
  for(int i=0;i<num_objects;i++)
  {
    auto object = runtime_get(objects, i);
    tangent_vector_length += object.algebra_dim();
  }

  Eigen::VectorXd tangent_vector(tangent_vector_length);

  int start = 0;
  for(int i=0;i<num_objects;i++)
  {
    auto object = runtime_get(objects, i);
    int dim = object.algebra_dim();

    tangent_vector.segment(start, dim) = decltype(object)::Log(object);
    start += dim;
  }

  return tangent_vector;
}

//////////////////////////////////////////////////////////////
// Helper Tuple Generation Functions
template <typename F, std::size_t... Is>
auto gen_tuple_impl(F func, std::index_sequence<Is...> ) {
    return std::make_tuple(func(Is)...);
}

template <std::size_t N, typename F>
auto gen_tuple(F func) {
  return gen_tuple_impl(func, std::make_index_sequence<N>{} );
}


//////////////////////////////////////////////////////////////
// This function converts an arbitrary set of uncertain lie
// group elements from some existing parameterization
// (ex. Euler Angle/Translation Params) to the Lie Algebra
// based representation.
//
// Currently only works for tuples of all the same type
//

template<typename LieGroup, std::size_t N>
//std::tuple< UncertainGroupElementRef<T, UncertainState<T...> >... >
auto 
convert_uncert_rep_from_coord_to_lie_algebra(
    const Eigen::VectorXd& mu,
    //    std::vector<Eigen::VectorXd>& mus,
    const Eigen::MatrixXd& Sigma,
    const std::function<LieGroup (const Eigen::VectorXd&)> conversion_function)
{
  static_assert(std::is_base_of<LieGroupBase, LieGroup>::value,
                "Template parameters for UncertainState must have LieGroupBase as base.");

  int total_dim = mu.rows();

  assert(total_dim == Sigma.rows());
  assert(total_dim == Sigma.cols());

  assert(total_dim % N == 0);
  int dim = total_dim/N;
  
  // Find Mean Values
  auto mu_new = gen_tuple<N> (
      [&](size_t i){ return conversion_function(mu.segment(i*dim, dim));});
  //auto mu_new = gen_tuple([&](std::size_t i){ return i;});

  //Transform Uncertainty Via Unscented Transform
  UT::UnscentedTransformInput ut_input(mu, Sigma);
  auto ut_result = UT::evaluate_unscented_transform(
      [&](const Eigen::VectorXd& point)
      {
        auto identity_centered_elements =
        gen_tuple<N> (
            [&](size_t i){
              LieGroup g = conversion_function(point.segment(i*dim, dim));
              LieGroup mean_inverse = conversion_function(mu.segment(i*dim, dim)).inverse();
              return Lie::compose(g, mean_inverse);
            });

        return log_tuple(identity_centered_elements);
      },
      ut_input);

  return Lie::make_uncertain_state(mu_new, ut_result.Sigma);   
}

}; // namespace Lie
#endif //__LIE_COORD_CHANGE_HPP

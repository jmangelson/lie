#ifndef __LIE_SO3_HPP
#define __LIE_SO3_HPP

#include <assert.h>
#include <iostream>

#include <Eigen/Core>

#include "lie.hpp"

namespace Lie
{

typedef Eigen::Matrix<double, 3, 3, Eigen::RowMajor> Matrix3dRow;

/////////////////////////////////////////
// Auxiliary Functions 
/////////////////////////////////////////
Matrix3dRow skew(const Eigen::Vector3d& v)
{
    Matrix3dRow v_mat;
    v_mat <<
           0, -v(2),  v(1),
        v(2),     0, -v(0),
       -v(1),  v(0),     0;

    return v_mat;
}

//////////////////////////////////////////////////////////
// The SO3 class represents an element of the SO(3) group
//////////////////////////////////////////////////////////
class SO3 : public LieGroup<SO3, 3>
{
 private:
  Matrix3dRow R_;
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  SO3()
      : R_()
  {
    R_ = Matrix3dRow::Identity();
  }
  
  SO3(const SO3& R)
      : R_(R())
  {}
  
  SO3(Eigen::Matrix3d R)
      : R_(R)
  {}

  // Assumes XYZ-roll, pitch, heading
  // Euler Angles Convention
  SO3(double r,
      double p,
      double h)
      : R_()
  {
    double sr = sin(r);
    double cr = cos(r);
    double sp = sin(p);
    double cp = cos(p);
    double sh = sin(h);
    double ch = cos(h);    
    
    R_ <<
        ch*cp, -sh*cr + ch*sp*sr,  sh*sr + ch*sp*cr,
        sh*cp,  ch*cr + sh*sp*sr, -ch*sr + sh*sp*cr,
        -sp,    cp*sr,             cp*cr;
  }

  ~SO3() {}

  SO3 inverse() const
  {
    SO3 inv(this->R_.transpose());
    return inv;
  }
  
  Matrix3dRow operator()() const
  {
    return R_;
  }

  // Multiplication Action
  SO3 operator*(const SO3& rhs) const
  {
    Matrix3dRow result = (*this)()*rhs();
    SO3 R = SO3(result);
    return R;
  }
  
  static SO3 Exp(const TangentVector& v)
  {
    double theta = sqrt( pow(v(0), 2.0) + pow(v(1), 2.0) + pow(v(2), 2.0) );

    if(pow(theta, 2.0) <= std::numeric_limits<double>::epsilon())
    {
      return SO3(Matrix3dRow::Identity());
    }
  
    auto a = v/theta;

    Matrix3dRow a_mat = skew(a);
  
    Matrix3dRow C =
        cos(theta)*Matrix3dRow::Identity() +
        (1-cos(theta))*a*a.transpose() +
        sin(theta) * a_mat;

    return SO3(C);
  }

  static TangentVector Log(const SO3& g)
  {
    double theta = acos((g().trace() - 1.)/2.);
  
    if(pow(theta, 2.0) <= std::numeric_limits<double>::epsilon())
    {
      Eigen::Vector3d zero = Eigen::Vector3d::Zero();
      return zero;
    }
  
    Eigen::Matrix3d w_mat = (theta/(2.*sin(theta)))*(g() - g().transpose());

    Eigen::Vector3d w;
    w << w_mat(2, 1), -w_mat(2, 0), w_mat(1, 0);

    return w;
  }

  static Eigen::Matrix<double, 3, 3> Ad(const SO3& g) {
    return g();
  }

  static Matrix3dRow J_l(const TangentVector& phi)
  {
    Eigen::Vector3d w = phi;
    double theta = sqrt( pow(w(0), 2.0) + pow(w(1), 2.0) + pow(w(2), 2.0) );

    if(pow(theta, 2.0) <= std::numeric_limits<double>::epsilon())
    {
      return Matrix3dRow::Identity();
    }

    auto a = w/theta;

    Matrix3dRow a_mat = skew(a);

    Matrix3dRow J =
        (sin(theta)/theta)*Matrix3dRow::Identity() +
        (1-sin(theta)/theta)*a*a.transpose() +
        ((1-cos(theta))/theta) * a_mat;
  
    return J;
  }

  static Matrix3dRow J_l_inv(const TangentVector& phi)
  {
    Eigen::Vector3d w = phi;
    double theta = sqrt( pow(w(0), 2.0) + pow(w(1), 2.0) + pow(w(2), 2.0) );

    if(pow(theta, 2.0) <= std::numeric_limits<double>::epsilon())
    {
      return Matrix3dRow::Identity();
    }

    auto a = w/theta;

    Matrix3dRow a_mat = skew(a);

    double cot_theta_half = cos(theta/2.0)/sin(theta/2.0);

    Matrix3dRow Jinv =
        (theta/2.0)*cot_theta_half*Matrix3dRow::Identity() +
        (1.0 - (theta/2.0)*cot_theta_half)*a*a.transpose() -
        (theta/2.0)*a_mat;
  
    return Jinv;
  }

  using LieGroup<SO3, 3>::inverse;
};

////////////////////////////////////
// Other Useful Operators
////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const SO3& rhs)  
{
  os << rhs();
  return os;
} 

Eigen::Vector3d operator*(const SO3& lhs, const Eigen::Vector3d& rhs)
{
  Eigen::Vector3d result = lhs()*rhs;
  return result;
}

Eigen::Vector3d operator*(const SO3& lhs, const Eigen::VectorXd& rhs)
{
  assert(rhs.rows() == 3);
  
  Eigen::Vector3d result = lhs()*rhs;
  return result;
}
  



}; // namespace Lie
#endif //__LIE_SO3_HPP


#include "lie/se3.hpp"
#include "mr/stat.hpp"

#include "lie_utils.hpp"

int main()
{
  double PI = 3.14159265359;  
  //  Lie::SE3 T_g1_mu (10, 10, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);

  Lie::SE3 T_g1_mu (3, 3, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);

  Eigen::VectorXd diag(6);
  diag << 0.01, 0.01, 0.00001, 0.00001, 0.00001, 0.03;
  Eigen::MatrixXd cov = diag.asDiagonal();
  cov(0,1) = 0;
  cov(1,0) = 0;
  //  cov = MR::STAT::rand_cov(6);

  auto T_g1 = Lie::make_uncertain_state(T_g1_mu, cov);
  auto T_g1_inverse = T_g1.inverse();
  

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(6);

  int N = 10000;
  Eigen::MatrixXd noise = MR::STAT::sample_mvg(zero, cov, N);

  std::vector<Lie::SE3> samples;
  samples.reserve(N);
  
  std::vector<Lie::SE3> inv_samples;
  inv_samples.reserve(N);


  std::vector<Lie::SE3> ident_samples;
  ident_samples.reserve(N);
  
  
  Eigen::MatrixXd post_inverse_locations(2, N);

  Eigen::MatrixXd noise_vectors_after_inverse(6, N);
  Eigen::MatrixXd noise_vectors_in_pose_2(6, N);

  for(int i=0;i<N;i++)
  {
    auto T_g1_sample = Lie::compose(Lie::SE3::Exp(noise.col(i)), T_g1_mu);

    samples.push_back(T_g1_sample);
    
    auto T_g1_sample_inverse = T_g1_sample.inverse();
    post_inverse_locations.col(i) << T_g1_sample_inverse.t()(0), T_g1_sample_inverse.t()(1);
    inv_samples.push_back(T_g1_sample_inverse());

    auto noisy_ident = Lie::compose(T_g1_inverse.mu(), T_g1_sample);
    ident_samples.push_back(noisy_ident);

    noise_vectors_after_inverse.col(i) = Lie::SE3::Log(T_g1_mu * T_g1_sample_inverse);
    noise_vectors_in_pose_2.col(i) = Lie::SE3::Log(noisy_ident);
  }

  std::cout << cov << "\n";

  
  std::cout << T_g1.mu() << "\n\n";

  
  std::cout << "\n\nEstimated Cov:\n" << T_g1_inverse.marginal_cov() << "\n\nEstimated Mean:\n" <<
      T_g1_inverse.mu() << "\n";

  std::cout << "\n\nSampled Cov :\n" << MR::STAT::mvg_cov(post_inverse_locations) << "\n\nSampled Mean:\n" <<
      MR::STAT::mvg_mu(post_inverse_locations) << "\n";

  std::cout << "\n\nSampled Cov After Inverse:\n" << MR::STAT::mvg_cov(noise_vectors_after_inverse) << "\n";
  
  std::cout << "\n\nSampled Cov In Pose 2 Frame:\n" << MR::STAT::mvg_cov(noise_vectors_in_pose_2) << "\n";
  

  
  //  LieUtils::plot_se3_elements_2d(ident_samples, "m");
  LieUtils::plot_frame(0,0,0, "r");

  //  LieUtils::plot_se3_elements_2d(samples);
  LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_g1, sqrt(20.06));  
  LieUtils::plot_frame(T_g1_mu);
  LieUtils::plot_se3_elements_2d(samples);  

  LieUtils::plot_se3_elements_2d(inv_samples, std::string("g"));
  //  LieUtils::plot_frame(inv_samples[0], "k");
   LieUtils::plot_frame(T_g1_inverse.mu(), std::string("g"));

  //  LieUtils::plot_frame(samples[0], "k");
  //  LieUtils::plot_frame(inv_samples[0], "k");
  // LieUtils::plot_frame(ident_samples[0], "k");


  

  
   LieUtils::plot_flattened_sigma_error_ellipsoid_5dsphere(T_g1_inverse, sqrt(20.06));

  // LieUtils::plot_sigma_error_ellipsoid(Lie::compose(T_g1_inverse.mu(), T_g1));    

  
  plt::axis("equal");

  plt::show();
}

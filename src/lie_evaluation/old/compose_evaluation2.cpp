#include "lie/se3.hpp"
#include "mr/stat.hpp"

#include "lie_utils.hpp"

Eigen::MatrixXd threshold_matrix(const Eigen::MatrixXd& mat, double min)
{
  auto new_mat = mat;
  for(int i=0;i<mat.rows();i++)
  {
    for(int j=0;j<mat.cols();j++)
    {
      if(fabs(mat(i, j)) < min )
        //         fabs(mat(i, j)) > max)
      {
        new_mat(i, j) = 0;
      }
    }
  }
  return new_mat;
}


int main()
{
  double PI = 3.14159265359;  
  //  Lie::SE3 T_g1_mu (10, 10, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);
  //  Lie::SE3 T_g2_mu (12, 12, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);
  Lie::SE3 T_g1_mu (3, 3, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);
  Lie::SE3 T_12_mu (1, 0, 0, 0.0, 0.0, PI/4.0);//-PI/4.0);    

  Eigen::VectorXd diag(6);
  diag << 0.005, 0.005, 0.00001, 0.00001, 0.00001, 0.006;
  Eigen::MatrixXd cov(12,12);
  cov.block(0,0,6,6) = diag.asDiagonal();
  cov.block(6,6,6,6) = diag.asDiagonal();
  Eigen::VectorXd cross(6);
  cross << 0.0045, 0.0045, 0, 0, 0, 0.005;
  cross(0, 1) = 0.0005;
  cross(1, 0) = 0.0005;
  cov.block(0,6,6,6) = cross.asDiagonal();
  cov.block(6,0,6,6) = cross.asDiagonal();
  //  cov = MR::STAT::rand_cov(12);


  auto refs = Lie::make_uncertain_state(std::make_tuple(T_g1_mu, T_12_mu), cov);
  auto T_g1 = std::get<0>(refs);
  auto T_12 = std::get<1>(refs);  

  Eigen::VectorXd zero = Eigen::VectorXd::Zero(12);

  int N = 10000;
  Eigen::MatrixXd noise = MR::STAT::sample_mvg(zero, cov, N);

  std::vector<Lie::SE3> samples_g1;
  samples_g1.reserve(N);
  std::vector<Lie::SE3> samples_12;
  samples_12.reserve(N);

  std::vector<Lie::SE3> samples_g2;
  samples_g2.reserve(N);  

  for(int i=0;i<N;i++)
  {
    auto T_g1_sample = Lie::compose(Lie::SE3::Exp(noise.col(i).head<6>()), T_g1_mu);
    auto T_12_sample = Lie::compose(Lie::SE3::Exp(noise.col(i).tail<6>()), T_12_mu);    

    samples_g1.push_back(T_g1_sample);
    samples_12.push_back(T_12_sample);    

    auto T_g2_sample = Lie::compose(T_g1_sample, T_12_sample);
    samples_g2.push_back(T_g2_sample);
  }

  LieUtils::plot_frame(0, 0, 0);
  
  LieUtils::plot_se3_elements_2d(samples_g1, "b");
  LieUtils::plot_se3_elements_2d(samples_12, "m");

  LieUtils::plot_flattened_sigma_error_ellipsoid_3dsphere(T_g1, sqrt(20.06), "violet" ,{{"linewidth", "2"}, {"alpha", "0.5"}}, "$\\mathbf{T}_{g1}$");
  LieUtils::plot_flattened_sigma_error_ellipsoid_3dsphere(T_12, sqrt(20.06), "m", {{"linewidth", "2"}, {"alpha", "0.5"}}, "$\\mathbf{T}_{12}$");

  LieUtils::plot_se3_elements_2d(samples_g2, "lightgrey");

  

  auto T_g1_indep = Lie::make_uncertain_state(T_g1.mu(), T_g1.marginal_cov());
  auto T_12_indep = Lie::make_uncertain_state(T_12.mu(), T_12.marginal_cov());  
  
  auto T_g2_indep = Lie::compose(T_g1_indep, T_12_indep);
  LieUtils::plot_flattened_sigma_error_ellipsoid_3dsphere(T_g2_indep, sqrt(20.06), "c", "2nd-Order w/o Correlation");

  auto T_g2 = Lie::compose(T_g1, T_12);
  LieUtils::plot_flattened_sigma_error_ellipsoid_3dsphere(T_g2, sqrt(20.06), "g", "2nd-Order with Correlation");
  
  plt::legend("lower right");
  plt::axis("equal");
  plt::xlabel("x (meters)");
  plt::ylabel("y (meters)");

  double num_valid_samples = 0;
  for(auto s : samples_g2)
  {
    auto error = Lie::between(T_g2.mu(), s);
    auto error_vec = Lie::SE3::Log(error);
    auto Information = threshold_matrix(T_g2.marginal_cov().inverse(), 1e-8);
    auto mahal_dist_squared = error_vec.transpose() * Information * error_vec;

    //    std::cout << mahal_dist_squared << "\n";
    if(mahal_dist_squared(0) < 13.197814645953381) //p = 0.96, dof=6 13.197814645953381
    {
      num_valid_samples++;
    }
  }

  std::cout << num_valid_samples;
  double perc_valid = num_valid_samples/N*100.0;
  std::cout << "Percent within 96% covariance bounds (with correlation): " << perc_valid << "\n";

  num_valid_samples = 0;
  for(auto s : samples_g2)
  {
    auto error = Lie::between(T_g2_indep.mu(), s);
    auto error_vec = Lie::SE3::Log(error);
    auto Information = threshold_matrix(T_g2_indep.marginal_cov().inverse(), 1e-8);
    auto mahal_dist_squared = error_vec.transpose() * Information * error_vec;

    //    std::cout << mahal_dist_squared << "\n";
    if(mahal_dist_squared(0) < 13.197814645953381) //p = 0.96, dof=6 13.197814645953381
    {
      num_valid_samples++;
    }
  }

  std::cout << num_valid_samples;
  perc_valid = num_valid_samples/N*100.0;
  std::cout << "Percent within 96% covariance bounds (w/o correlation): " << perc_valid << "\n";

  
  plt::show();
}

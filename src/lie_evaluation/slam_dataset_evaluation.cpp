///////////////////////////////////////////////
// Generates Figures 10-14 and Tables I and II


#include "lie/se3.hpp"
#include "lie/coord_change.hpp"

#include "mr/stat.hpp"
#include "mr/ct.hpp"

#include "cereal/cereal.hpp"
#include <cereal/archives/binary.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/tuple.hpp>
#include <fstream>
#include "cerealize_eigen_matrix.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "lie_utils.hpp"

#include <string>
#include <sstream>
#include <limits.h>
#include <unistd.h>
#include <math.h>
#include <map>
#include <cmath>

std::string getexepath()
{
  char result[ PATH_MAX ];
  ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
  return std::string( result, (count > 0) ? count : 0 );
}

double covariance_error(const Eigen::MatrixXd& Sigma_MC, const Eigen::MatrixXd& Sigma_test, bool normalize)
{
  if(normalize)
  {
    double frob_mc = sqrt( ( Sigma_MC*Sigma_MC.transpose() ).trace() );
  
    return sqrt( ( (Sigma_test/frob_mc - Sigma_MC/frob_mc).transpose() * (Sigma_test/frob_mc - Sigma_MC/frob_mc) ).trace() );
  }
  else
  {
    return sqrt( ( (Sigma_test - Sigma_MC).transpose() * (Sigma_test - Sigma_MC) ).trace() );
  }
}

///////////////////////////
// Cereal Serialization
class PoseData
{
 public:
  std::vector< Eigen::MatrixXd > final_poses;
  
  std::list< std::tuple<unsigned int, unsigned int> > indices;
  std::list< Eigen::MatrixXd > relative_poses;

  template<class Archive>
  void serialize(Archive & archive)
  {
    archive( final_poses, indices, relative_poses );
  }
};
  
class CovData
{
 public:
  std::list< std::tuple<unsigned int, unsigned int> > indices;

  std::list< Eigen::MatrixXd > covariance_blocks;

  template<class Archive>
  void serialize(Archive & archive)
  {
    archive( indices, covariance_blocks );
  }
};

////////////////////////////////////////////////////
// Conversion Functions

Lie::SE3 xyzrph_to_SE3(const Eigen::VectorXd& xyzrph)
{
  Lie::SE3 H(xyzrph(0),
             xyzrph(1),
             xyzrph(2),
             xyzrph(3),
             xyzrph(4),
             xyzrph(5));
  return H;
}

Eigen::VectorXd SE3_to_xyzrph(Lie::SE3& H)
{
  double x, y, z, r, p, h;

  auto t = H.t();
  x = t(0); y = t(1); z = t(2);
  auto R = H.R()();
  h = atan2(R(1,0), R(0,0));
  double sh = sin(h);
  double ch = cos(h);
  p = atan2(-R(2,0), R(0,0)*ch + R(1,0)*sh);
  r = atan2 (R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);

  Eigen::VectorXd xyzrph(6);
  xyzrph << x, y, z, r, p, h;

  return xyzrph;
}

Lie::SE3 SE2_mat_to_SE3(const Eigen::MatrixXd& H)
{
  double x, y, z, r, p, h;
  z = 0;
  r = 0;
  p = 0;

  x = H(0,2);
  y = H(1,2);
  h = atan2(H(1,0), H(0,0));

  return Lie::SE3(x, y, z, r, p, h);
}

Lie::SE3 SE2_vec_to_SE3(const Eigen::VectorXd& xyt)
{
  double x, y, z, r, p, h;
  z = 0;
  r = 0;
  p = 0;

  x = xyt(0);
  y = xyt(1);
  h = xyt(2);

  return Lie::SE3(x, y, z, r, p, h);
}





///////////////////////////////////////////////////////
// Evaluation Function

Eigen::MatrixXd expand_2D_covariance(const Eigen::MatrixXd& cov_6x6)
{
  Eigen::MatrixXd cov_12x12 = Eigen::MatrixXd::Zero(12,12);
  int a, b, c, d;
  a = b = 0; c = d = 0;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = 6; b = 0; c = 3; d = 0;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = 0; b = 6; c = 0; d = 3;  
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = b = 6; c = d = 3;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  return cov_12x12;
}

Eigen::MatrixXd
calculate_mc_relative_pose_cov(
    const Eigen::MatrixXd& H_g1_est,
    const Eigen::MatrixXd& H_g2_est,
    const Eigen::MatrixXd& Sigma_g1g2_est)
{
  int NUM_SAMPLES = 1000;
  Eigen::MatrixXd Sigma_g1g2 = expand_2D_covariance(Sigma_g1g2_est);
  Eigen::VectorXd zero = Eigen::VectorXd::Zero(12);
  Eigen::MatrixXd xi = MR::STAT::sample_mvg(zero, Sigma_g1g2, NUM_SAMPLES);

  auto T_g1_mu = SE2_mat_to_SE3(H_g1_est);
  auto T_g2_mu = SE2_mat_to_SE3(H_g2_est);  

  auto T_12_mu = Lie::between(T_g1_mu, T_g2_mu);


  Eigen::MatrixXd xi_samples(6, NUM_SAMPLES);
  
  for(int i=0;i<NUM_SAMPLES;i++)
  {
    auto T_g1_sample = Lie::compose( Lie::SE3::Exp(xi.col(i).head<6>()),
                                     T_g1_mu );
    auto T_g2_sample = Lie::compose( Lie::SE3::Exp(xi.col(i).tail<6>()),
                                     T_g2_mu );

    auto T_12_sample = Lie::between( T_g1_sample, T_g2_sample );

    Eigen::VectorXd xi_sample = Lie::SE3::Log(Lie::compose(T_12_sample, T_12_mu.inverse()));
    xi_samples.col(i) = xi_sample;
  }

  Eigen::MatrixXd Sigma_12_MC = MR::STAT::mvg_cov(xi_samples);
  return Sigma_12_MC;
}

Eigen::MatrixXd
calculate_mc_relative_pose_cov_xyzrph(
    const Eigen::MatrixXd& H_g1_est,
    const Eigen::MatrixXd& H_g2_est,
    const Eigen::MatrixXd& Sigma_g1g2_est)
{
  int NUM_SAMPLES = 1000;
  Eigen::MatrixXd Sigma_g1g2 = expand_2D_covariance(Sigma_g1g2_est);
  Eigen::VectorXd zero = Eigen::VectorXd::Zero(12);
  Eigen::MatrixXd xi = MR::STAT::sample_mvg(zero, Sigma_g1g2, NUM_SAMPLES);

  auto T_g1_mu = SE2_mat_to_SE3(H_g1_est);
  auto T_g2_mu = SE2_mat_to_SE3(H_g2_est);  

  //  auto T_12_mu = Lie::between(T_g1_mu, T_g2_mu);


  Eigen::MatrixXd xyzrph_samples(6, NUM_SAMPLES);
  
  for(int i=0;i<NUM_SAMPLES;i++)
  {
    auto T_g1_sample = Lie::compose( Lie::SE3::Exp(xi.col(i).head<6>()),
                                     T_g1_mu );
    auto T_g2_sample = Lie::compose( Lie::SE3::Exp(xi.col(i).tail<6>()),
                                     T_g2_mu );

    auto T_12_sample = Lie::between( T_g1_sample, T_g2_sample );

    Eigen::VectorXd xyzrph_sample = SE3_to_xyzrph(T_12_sample);
    xyzrph_samples.col(i) = xyzrph_sample;
  }

  Eigen::MatrixXd Sigma_12_MC = MR::STAT::mvg_cov(xyzrph_samples);
  return Sigma_12_MC;
}


Eigen::MatrixXd 
calculate_relative_pose_cov_independent(
    const Eigen::MatrixXd& H_g1_est,
    const Eigen::MatrixXd& H_g2_est,
    const Eigen::MatrixXd& Sigma_g1g2_est)
{

  auto T_g1_mu = SE2_mat_to_SE3(H_g1_est);
  auto T_g2_mu = SE2_mat_to_SE3(H_g2_est);

  Eigen::MatrixXd Sigma_g1g2 = expand_2D_covariance(Sigma_g1g2_est);

  auto T_g1 = Lie::make_uncertain_state(T_g1_mu, Sigma_g1g2.topLeftCorner(6,6));
  auto T_g2 = Lie::make_uncertain_state(T_g2_mu, Sigma_g1g2.bottomLeftCorner(6,6));

  auto T_12 = Lie::between(T_g1, T_g2);

  return T_12.marginal_cov();
}

Eigen::MatrixXd 
calculate_relative_pose_cov_joint(
    const Eigen::MatrixXd& H_g1_est,
    const Eigen::MatrixXd& H_g2_est,
    const Eigen::MatrixXd& Sigma_g1g2_est)
{
  auto T_g1_mu = SE2_mat_to_SE3(H_g1_est);
  auto T_g2_mu = SE2_mat_to_SE3(H_g2_est);
  
  Eigen::MatrixXd Sigma_g1g2 = expand_2D_covariance(Sigma_g1g2_est);

  auto joint_state = Lie::make_uncertain_state(
      std::make_tuple(T_g1_mu, T_g2_mu), Sigma_g1g2);

  auto T_g1 = std::get<0>(joint_state);
  auto T_g2 = std::get<1>(joint_state);  

  auto T_12 = Lie::between(T_g1, T_g2);

  return T_12.marginal_cov();
}

Eigen::MatrixXd 
calculate_relative_pose_cov_ssc(
    const Eigen::MatrixXd& H_g1_est,
    const Eigen::MatrixXd& H_g2_est,
    const Eigen::MatrixXd& Sigma_g1g2_ssc)
{
  auto T_g1_mu = SE2_mat_to_SE3(H_g1_est);
  auto T_g2_mu = SE2_mat_to_SE3(H_g2_est);

  Eigen::VectorXd x_g1 = SE3_to_xyzrph(T_g1_mu);
  Eigen::VectorXd x_g2 = SE3_to_xyzrph(T_g2_mu);  
  //  x_g1g2 << SE3_to_xyzrph(T_g1_mu), SE3_to_xyzrph(T_g2_mu);
  Eigen::MatrixXd Sigma_g1g2 = expand_2D_covariance(Sigma_g1g2_ssc);

  Eigen::MatrixXd J = MR::SSC::tail2tail_jac(x_g1, x_g2);

  Eigen::MatrixXd Sigma_12 = J * Sigma_g1g2 * J.transpose();

  return Sigma_12;
}

///////////////////////////////////////////////////////
// Plot Trajectories
void plot_2d_trajectory(const PoseData& p_data)
{
  std::vector<double> x;
  std::vector<double> y;

  int i = 0;
  
  for(auto& p : p_data.final_poses)
  {
    x.push_back(p(0,2));
    y.push_back(p(1,2));
    i++;
  }
  
  plt::plot(x,y);
}

///////////////////////////////////////////////////////
// Plot Correlation

double pearsons_correlation(double var_a,
                            double var_b,
                            double cov_ab)
{
  return cov_ab/(sqrt(var_a)*sqrt(var_b));
}

std::tuple<double, double, double>
HSL_to_RGB(double h, double s, double l)
{
  double c = (1 - abs(2*l -1))*s;
  double h_p = h/60;
  double x = c * (1 - abs( fmod(h_p, 2.0) - 1));
  double r1, g1, b1;
  if(h > 360 || h < 0)
  {
    r1 = g1 = b1 = 0;
  }
  else if(h_p >= 0 && h_p <= 1)
  {
    r1 = c; g1 = x; b1 = 0;
  }
  else if(h_p >= 1 && h_p <= 2)
  {
    r1 = x; g1 = c; b1 = 0;
  }
  else if(h_p >= 2 && h_p <= 3)
  {
    r1 = 0; g1 = c; b1 = x;
  }
  else if(h_p >= 3 && h_p <= 4)
  {
    r1 = 0; g1 = x; b1 = c;
  }
  else if(h_p >= 4 && h_p <= 5)
  {
    r1 = x; g1 = 0; b1 = c;
  }
  else 
  {
    r1 = c; g1 = 0; b1 = x;
  }

  double m = l - c/2;
  return std::make_tuple(r1 + m,
                         g1 + m,
                         b1 + m);
}

std::tuple<double, double, double>
HSV_to_RGB(double h, double s, double v)
{
  double c = v*s;
  double h_p = h/60;
  double x = c * (1 - abs( fmod(h_p, 2.0) - 1));
  double r1, g1, b1;
  if(h > 360 || h < 0)
  {
    r1 = g1 = b1 = 0;
  }
  else if(h_p >= 0 && h_p <= 1)
  {
    r1 = c; g1 = x; b1 = 0;
  }
  else if(h_p >= 1 && h_p <= 2)
  {
    r1 = x; g1 = c; b1 = 0;
  }
  else if(h_p >= 2 && h_p <= 3)
  {
    r1 = 0; g1 = c; b1 = x;
  }
  else if(h_p >= 3 && h_p <= 4)
  {
    r1 = 0; g1 = x; b1 = c;
  }
  else if(h_p >= 4 && h_p <= 5)
  {
    r1 = x; g1 = 0; b1 = c;
  }
  else 
  {
    r1 = c; g1 = 0; b1 = x;
  }

  double m = v-c;
  return std::make_tuple(r1 + m,
                         g1 + m,
                         b1 + m);
}


std::tuple<double, double, double>
corr_to_color(double corr)
{
  assert(corr >= 0 );
  assert(corr <= 1 );  

  auto rgb = HSV_to_RGB(270, 0, 1-corr);
  return rgb;
}

std::tuple<double, double, double>
error_to_color(double error, double max_error, double hue)
{
  double val = error/max_error;

  if(val > 1) val = 1;
      
  assert(val >= 0 );
  assert(val <= 1 );  

  auto rgb = HSL_to_RGB(hue, 1, 1-val*0.75);        
  return rgb;
}

template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v, bool flip_order=false) {

  std::vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  if(!flip_order)
  {
    sort(idx.begin(), idx.end(),
         [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});
  }
  else
  {
    sort(idx.begin(), idx.end(),
         [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});
  }



  return idx;
}

void plot_2d_correlation(
    const PoseData& p_data,
    std::vector< std::tuple<unsigned int, unsigned int, Eigen::MatrixXd> > pose_data,
    int offset=0)
{
  plt::figure_size(600,300);

  std::vector<double> correlation;

  for(auto t : pose_data)
  {
    auto joint_cov = std::get<2>(t);

    double var_a = joint_cov(offset, offset);
    double var_b = joint_cov(offset+3, offset+3);
    double cov_ab = joint_cov(offset, offset+3);

    double corr = pearsons_correlation(var_a, var_b, cov_ab);
    correlation.push_back(corr);
  }

  for(auto i : sort_indexes(correlation))
  {
    auto pair = pose_data[i];
    auto id1 = std::get<0>(pair);
    auto id2 = std::get<1>(pair);

    std::vector<double> x = {p_data.final_poses[id1](0,2), p_data.final_poses[id2](0,2)};
    std::vector<double> y = {p_data.final_poses[id1](1,2), p_data.final_poses[id2](1,2)};

    auto corr = correlation[i];

    plt::plot_color(x, y, corr_to_color(abs(corr)));
  }

  plot_2d_trajectory(p_data);
}


void plot_covariance_error(
    const PoseData& p_data,
    std::vector< std::tuple<unsigned int, unsigned int, Eigen::MatrixXd> > pose_data,
    std::vector<double> cov_error,
    double max_error,
    double hue)
{
  plt::figure_size(600,300);  

  for(auto i : sort_indexes(cov_error))
  {
    auto pair = pose_data[i];
    auto id1 = std::get<0>(pair);
    auto id2 = std::get<1>(pair);

    std::vector<double> x = {p_data.final_poses[id1](0,2), p_data.final_poses[id2](0,2)};
    std::vector<double> y = {p_data.final_poses[id1](1,2), p_data.final_poses[id2](1,2)};

    plt::plot_color(x, y, error_to_color(abs(cov_error[i]), max_error, hue));
  }

  plot_2d_trajectory(p_data);
}


std::vector< std::vector<double> >
evaluate_correctness(const std::string& gt_pose_datafile,
                     const std::string& dataset_pose_datafile,                          
                     const std::string& dataset_lie_cov_datafile,
                     const std::string& dataset_ssc_cov_datafile,
                     int pose_offset,
                     bool plot,
                     double max_error_indep,
                     double max_error_ssc)    
{

  std::cout << "Pose Offset: " << pose_offset << " =========================================================\n";
  if(plot)
    
    std::cout << "Plot is True\n";
  
  //////////////////////////////////////////////////////////////////////////
  // LOAD DATA
  //////////////////////////////////////////////////////////////////////////
  
  /////////////////////////////////////////
  // Load groundtruth pose data
  std::cout << "Reading gt pose data...";
  std::string path = getexepath();
  path.erase(path.end()-23, path.end());
  std::string filename = path + gt_pose_datafile;
  std::cout << filename << "\n";
  std::ifstream in(filename.c_str(), std::ios::in | std::ios::binary);

  PoseData gt_pose_data;
  {
    cereal::BinaryInputArchive iarchive(in);
    iarchive(gt_pose_data);
  }

  in.close();
  std::cout << "Finished.\n";

  /////////////////////////////////////////
  // Load dataset pose data
  std::cout << "Reading dataset pose data...";
  filename = path + dataset_pose_datafile;
  std::cout << filename << "\n";
  std::ifstream in1(filename.c_str(), std::ios::in | std::ios::binary);

  PoseData pose_data;
  {
    cereal::BinaryInputArchive iarchive(in1);
    iarchive(pose_data);
  }

  in1.close();
  std::cout << "Finished.\n";

  /////////////////////////////////////////
  // Load covariance data (Lie Algebra)
  std::cout << "Reading Lie covariance data...";
  filename = path + dataset_lie_cov_datafile;
  std::cout << filename << "\n";
  std::ifstream in2(filename.c_str(), std::ios::in | std::ios::binary);                   

  CovData cov_data_lie;
  {
    cereal::BinaryInputArchive iarchive(in2);
    iarchive(cov_data_lie);
  }

  in2.close();
  std::cout << "Finished.\n";

  /////////////////////////////////////////
  // Load covariance data (SSC)
  std::cout << "Reading SSC covariance data...";
  filename = path + dataset_ssc_cov_datafile;
  std::cout << filename << "\n";
  std::ifstream in3(filename.c_str(), std::ios::in | std::ios::binary);                   

  CovData cov_data_ssc;
  {
    cereal::BinaryInputArchive iarchive(in3);
    iarchive(cov_data_ssc);
  }

  in3.close();
  std::cout << "Finished.\n";

  std::vector< std::tuple<unsigned int, unsigned int, Eigen::MatrixXd> > used_pairs;

  std::vector<double> joint_cov_error;
  std::vector<double> indep_cov_error;

  std::vector<double> joint_cov_error_norm;  
  std::vector<double> ssc_cov_error;  
  int i=0;

  while(! pose_data.indices.empty())
  {
    auto id1 = std::get<0>(pose_data.indices.front());
    auto id2 = std::get<1>(pose_data.indices.front());

    double diff = id2 - id1;
    if(abs(diff) != pose_offset)
    {
      pose_data.indices.pop_front();
      gt_pose_data.relative_poses.pop_front();
      cov_data_lie.covariance_blocks.pop_front();
      cov_data_ssc.covariance_blocks.pop_front();
      continue;
    }

    if(i % 100 == 0)
    {
      std::cout << i << ", " << std::flush;
    }

    // Calculate MC Cov for Pair
    Eigen::MatrixXd MC_cov = calculate_mc_relative_pose_cov(pose_data.final_poses[id1],
                                                            pose_data.final_poses[id2],
                                                            cov_data_lie.covariance_blocks.front());

    // Calculate Joint Cov for Pair
    Eigen::MatrixXd Joint_cov = calculate_relative_pose_cov_joint(pose_data.final_poses[id1],
                                                                  pose_data.final_poses[id2],
                                                                  cov_data_lie.covariance_blocks.front());

    // Calculate Indep Cov for Pair
    Eigen::MatrixXd Indep_cov = calculate_relative_pose_cov_independent(pose_data.final_poses[id1],
                                                                        pose_data.final_poses[id2],
                                                                        cov_data_lie.covariance_blocks.front());

    // Calculate MC Cov for Pair SSC
    Eigen::MatrixXd MC_cov_ssc = calculate_mc_relative_pose_cov_xyzrph(pose_data.final_poses[id1],
                                                                       pose_data.final_poses[id2],
                                                                       cov_data_lie.covariance_blocks.front());

    // Calculate SSC Cov for Pair
    Eigen::MatrixXd SSC_cov = calculate_relative_pose_cov_ssc(pose_data.final_poses[id1],
                                                              pose_data.final_poses[id2],
                                                              cov_data_ssc.covariance_blocks.front());    


    // Evaluate Error
    double pair_joint_cov_error = covariance_error(MC_cov, Joint_cov, false);
    double pair_indep_cov_error = covariance_error(MC_cov, Indep_cov, false);

    double pair_joint_cov_error_norm = covariance_error(MC_cov, Joint_cov, true);
    double pair_ssc_cov_error = covariance_error(MC_cov_ssc, SSC_cov, true);        

    joint_cov_error.push_back(pair_joint_cov_error);
    indep_cov_error.push_back(pair_indep_cov_error);

    joint_cov_error_norm.push_back(pair_joint_cov_error_norm);    
    ssc_cov_error.push_back(pair_ssc_cov_error);    
    
    used_pairs.push_back(
        std::make_tuple(id1, id2, cov_data_lie.covariance_blocks.front()));

    pose_data.indices.pop_front();
    gt_pose_data.relative_poses.pop_front();
    cov_data_lie.covariance_blocks.pop_front();
    cov_data_ssc.covariance_blocks.pop_front();
    i++;
  }
  std::cout << i << "\n";

  if(plot)
  {
    plot_2d_correlation(pose_data, used_pairs, 0);
    plot_2d_correlation(pose_data, used_pairs, 1);
    plot_2d_correlation(pose_data, used_pairs, 2);

    plot_covariance_error(pose_data, used_pairs, joint_cov_error, max_error_indep, 0);
    plot_covariance_error(pose_data, used_pairs, indep_cov_error, max_error_indep, 0);

    plot_covariance_error(pose_data, used_pairs, joint_cov_error_norm, max_error_ssc, 30);
    plot_covariance_error(pose_data, used_pairs, ssc_cov_error, max_error_ssc, 30);        
  }

  std::vector< std::vector<double> > result;
  result.push_back(joint_cov_error);
  result.push_back(indep_cov_error);
  result.push_back(joint_cov_error_norm);  
  result.push_back(ssc_cov_error);
  return result;
}

double std_dev(const std::vector<double> data)
{
  double mean = std::accumulate( data.begin(), data.end(), 0.0)/data.size();
  
  double var = 0;
  for( int n = 0; n < data.size(); n++ )
  {
    var += (data[n] - mean) * (data[n] - mean);
  }
  var /= data.size();
  return sqrt(var);
}
    
    

int main()
{
  std::vector<double> eval_pose_offsets = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 200, 500};
  //std::vector<double> eval_pose_offsets = {50, 100};

  std::vector<double> joint_error;
  std::vector<double> indep_error;
  std::vector<double> joint_error_norm;  
  std::vector<double> ssc_error;

  // std::map<double, double> indep_max_error;
  // std::map<double, double> ssc_max_error;  

  for(int i=0;i<eval_pose_offsets.size();i++)
  {
    auto result = 
        evaluate_correctness("../../test_data/manhattanOlson3500_groundtruth.txt_pose_data.cereal",
                             "../../test_data/manhattanOlson3500.txt_pose_data.cereal",
                             "../../test_data/manhattanOlson3500.txt_covariances.cereal_lie",
                             "../../test_data/manhattanOlson3500.txt_covariances.cereal",
                             eval_pose_offsets[i],
                             false,
                             0,
                             0);

    for(auto error : result[0])
      joint_error.push_back(error);
    for(auto error : result[1])
      indep_error.push_back(error);
    for(auto error : result[2])
      joint_error_norm.push_back(error);    
    for(auto error : result[3])
      ssc_error.push_back(error);

    // double mean_indep_error = std::accumulate( result[1].begin(), result[1].end(), 0.0)/result[1].size();
    // double stddev_indep_error = std_dev(result[1]);

    // double mean_ssc_error = std::accumulate( result[3].begin(), result[3].end(), 0.0)/result[3].size();
    // double stddev_ssc_error = std_dev(result[3]);

    // double MAX_ERROR_INDEP = 2.0*stddev_indep_error + mean_indep_error;
    // double MAX_ERROR_SSC = 2.0*stddev_ssc_error + mean_ssc_error;
    // indep_max_error[eval_pose_offsets[i]] = MAX_ERROR_INDEP;
    // ssc_max_error[eval_pose_offsets[i]] = MAX_ERROR_SSC;    
  }

  double mean_joint_error = std::accumulate( joint_error.begin(), joint_error.end(), 0.0)/joint_error.size();
  double stddev_joint_error = std_dev(joint_error);

  double mean_indep_error = std::accumulate( indep_error.begin(), indep_error.end(), 0.0)/indep_error.size();
  double stddev_indep_error = std_dev(indep_error);

  double mean_joint_error_norm = std::accumulate( joint_error_norm.begin(), joint_error_norm.end(), 0.0)/joint_error_norm.size();
  double stddev_joint_error_norm = std_dev(joint_error_norm);
  

  double mean_ssc_error = std::accumulate( ssc_error.begin(), ssc_error.end(), 0.0)/ssc_error.size();
  double stddev_ssc_error = std_dev(ssc_error);  

  

  // Max error for color scaling set at 2 std deviations above the mean
  double MAX_ERROR_INDEP = 2.0*stddev_indep_error + mean_indep_error;
  double MAX_ERROR_SSC = 2.0*stddev_ssc_error + mean_ssc_error;
  if(MAX_ERROR_INDEP == 0 || std::isnan(MAX_ERROR_INDEP)) MAX_ERROR_INDEP = 7.5;
  if(MAX_ERROR_SSC == 0 || std::isnan(MAX_ERROR_SSC)) MAX_ERROR_SSC = 1.12;  

  // Plot Correlation and Error
  std::vector<double> pose_offsets = {5, 10, 50, 100, 200, 500};
  
  for(int i=0;i<pose_offsets.size();i++)
  {
    auto result = 
        evaluate_correctness("../../test_data/manhattanOlson3500_groundtruth.txt_pose_data.cereal",
                             "../../test_data/manhattanOlson3500.txt_pose_data.cereal",
                             "../../test_data/manhattanOlson3500.txt_covariances.cereal_lie",
                             "../../test_data/manhattanOlson3500.txt_covariances.cereal",
                             pose_offsets[i],
                             true,
                             MAX_ERROR_INDEP,
                             MAX_ERROR_SSC);
    //                             indep_max_error[pose_offsets[i]],
    //                             ssc_max_error[pose_offsets[i]]);
  }

  std::cout << "=========================================================\n";  
  std::cout << "Joint Error Mean: " << mean_joint_error << "\n";
  std::cout << "Joint Error StdDev: " << stddev_joint_error << "\n";

  std::cout << "Indep Error Mean: " << mean_indep_error << "\n";
  std::cout << "Indep Error StdDev: " << stddev_indep_error << "\n";

  std::cout << "=========================================================\n";
  std::cout << "Normalized Joint Error Mean: " << mean_joint_error_norm << "\n";
  std::cout << "Normalized Joint Error StdDev: " << stddev_joint_error_norm << "\n";
  
  std::cout << "Normalized SSC Error Mean: " << mean_ssc_error << "\n";
  std::cout << "Normalized SSC Error StdDev: " << stddev_ssc_error << "\n";

  std::cout << "=========================================================\n";
  std::cout << "Num Pose Pairs: " << joint_error.size() << "\n";
  std::cout << "Max Error for Color Scale (Indep): " << MAX_ERROR_INDEP << "\n";
  std::cout << "Max Error for Color Scale (SSC): " << MAX_ERROR_SSC << "\n";        

  plt::show();
}

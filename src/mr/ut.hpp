//ut.hpp
//Unscented Transform

#ifndef UT_HPP
#define UT_HPP

#include <Eigen/Core>

namespace MR
{
  namespace UT
  {
    typedef struct _ut_t ut_t;
    struct _ut_t 
    {
      Eigen::MatrixXd sigma_points;
      Eigen::VectorXd mean_weight;
      Eigen::VectorXd cov_weight;
    };

    void unscented_transform (ut_t *ut, const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma,
                              double alpha=1.0, double kappa=0.0, double beta=2.0);

    typedef struct _ut_return_t ut_return_t;
    struct _ut_return_t
    {
      Eigen::VectorXd mu_prime;
      Eigen::MatrixXd sigma_prime;
    };

    typedef const Eigen::VectorXd& InVectorRef;
    typedef Eigen::VectorXd OutVector;
    void unscented_func (ut_return_t *ut_ret, OutVector (*f)(InVectorRef, void *), ut_t * ut, 
                         void * args, bool angle_map[]=NULL);
  };
};
#endif


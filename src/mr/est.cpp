#include <iostream>
#include <cmath>

#include <Eigen/Dense>

#include "est.hpp"
#include "diff.hpp"
#include "misc.hpp"

/***********************************
 * Helpful Linear Algebra Functions
 ***********************************/

Eigen::MatrixXd MR::EST::LIN_ALG::matrix_inversion_lemma(const Eigen::MatrixXd& A_inv, const Eigen::MatrixXd& B, 
                                                       const Eigen::MatrixXd& C, const Eigen::MatrixXd& D){
  
  assert(C.rows() == C.cols());
  assert(A_inv.cols() == B.rows());
  assert(D.cols() == A_inv.rows());
  assert(C.rows() == D.rows());
  assert(C.cols() == B.cols());

  Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(C);
  assert(lu_decomp.isInvertible());
  Eigen::MatrixXd C_inv = lu_decomp.inverse();
  
  return A_inv - A_inv*B*(C_inv + D*A_inv*B).inverse()*D*A_inv;
}

/************************************
 * Estimator Base Class
 ************************************/

MR::EST::Est_Base::Est_Base(int state_dim, bool angle_map[]){
  dim = state_dim;
  initialized = false;

  if(angle_map == NULL)
    this->angle_map = NULL;
  else{
    this->angle_map = (bool *)malloc(state_dim*sizeof(bool));
    for(int i=0;i<state_dim;i++)
      this->angle_map[i] = angle_map[i];
  }  
}

MR::EST::Est_Base::~Est_Base()
{
  if(angle_map != NULL)
    free(angle_map);
}

void MR::EST::Est_Base::clear()
{
  initialized = false;
}

/*************************************
 * Extended Kalman Filter
 *************************************/
/*(MR::EST::EKF::EKF(int state_dim, bool angle_map[]) : Est_Base(state_dim, angle_map)
{
  NIS_threash = -1;
  }*/

MR::EST::EKF::EKF(int state_dim, bool angle_map[], double NIS_threashold, double innovation_threashold[]) : Est_Base(state_dim, angle_map)
{
  NIS_threash = NIS_threashold;
  if(innovation_threashold == NULL)
    innov_bound = false;
  else{
    innov_threash = (double *)calloc(state_dim, sizeof(double));
    innov_bound = true;
    for(int i=0;i<state_dim;i++)
      innov_threash[i] = innovation_threashold[i];
  }
}

void MR::EST::EKF::init(const Eigen::VectorXd& x_0, const Eigen::MatrixXd& P_0)
{
  x_k = x_0;
  P_k = P_0;
  initialized = true;
}

void MR::EST::EKF::predict( ProcessModel f, void *args, const Eigen::MatrixXd& Q_k)
{
  assert(initialized);

  Eigen::VectorXd x_kplus1 = f(x_k, args);

  //Differentiate f with respect to x_k
  Eigen::MatrixXd F_k = DIFF::num_jacob_central(f, x_k, args);
  
  Eigen::MatrixXd P_kplus1 = F_k*P_k*F_k.transpose() + Q_k;

  if(angle_map==NULL)
    x_k = x_kplus1;
  else
    x_k = minimized_angle_vec(x_kplus1, angle_map);
  P_k = P_kplus1;
}

double MR::EST::EKF::update( ObservationModel h, void *args, const Eigen::VectorXd& z_k, 
                       const Eigen::MatrixXd& R_k)
{
  assert(initialized);
  
  //Calculate Innovation
  Eigen::VectorXd y_hat = z_k - h(x_k, args);
  if(angle_map != NULL)
    y_hat = minimized_angle_vec(y_hat, angle_map);

  //Differentiate h with respect to x_k
  Eigen::MatrixXd H_k = DIFF::num_jacob_central(h, x_k, args);

  //Calculate Measurement Covariance
  Eigen::MatrixXd S_k = H_k*P_k*H_k.transpose() + R_k;

  Eigen::MatrixXd S_k_inv = (S_k.inverse());

  //Filter Outlier based on NIS
  std::cout << "Innovation:" << y_hat.transpose()<< "\n";
  double NIS = y_hat.transpose()*(S_k_inv)*y_hat;
  std::cout << "NIS: " << NIS << "\n";
  if(NIS_threash != -1){
    //    double NIS = y_hat.transpose()*(S_k_inv)*y_hat;
    if(NIS > NIS_threash){
      return NIS;
    }
  }
  
  //Filter Based on Innovation
  if(innov_bound == true){
    for(int i=0;i<dim;i++){
      if(( y_hat[i]>0 && y_hat[i] > innov_threash[i] ) ||
         ( y_hat[i]<0 && y_hat[i] < -innov_threash[i] ) ){
        std::cout << "Filtering Outlier based on Innovation element " << i << ", Threash: " << innov_threash[i] << ", Innovation Value: " << y_hat[i] << "\n";
        return NIS;
      }
    }
  }

  //Calculate Kalman Gain
  Eigen::MatrixXd K_k = P_k*(H_k.transpose())*(S_k_inv);
  
  //Update
  Eigen::MatrixXd P_kplus1 = (Eigen::MatrixXd::Identity(dim, dim) - K_k*H_k)*P_k;
  Eigen::MatrixXd x_kplus1 = x_k + K_k*y_hat;

  P_k = P_kplus1;
  if(angle_map==NULL)
    x_k = x_kplus1;
  else
    x_k = minimized_angle_vec(x_kplus1, angle_map);

  return NIS;
}

/**************************************
 * Weighted Least Squares Filter
 **************************************/
MR::EST::WLS::WLS(int state_dim, bool angle_map[]) : Est_Base(state_dim, angle_map) 
{  
  Q_k = Eigen::MatrixXd::Zero(dim, dim);
  Gamma_n = Eigen::VectorXd::Zero(dim);

  //Does Not currently work for angles
  if(angle_map != NULL)
    for(int i=0;i<state_dim;i++)
      assert(angle_map[i] == false);
};


void MR::EST::WLS::clear()
{
  Est_Base::clear();
  Q_k = Eigen::MatrixXd::Zero(dim, dim);
  Gamma_n = Eigen::VectorXd::Zero(dim);
}

void MR::EST::WLS::add_measurement(const Eigen::VectorXd& y_kplus1, 
                               const Eigen::MatrixXd& S_kplus1,
                               const Eigen::MatrixXd& C_kplus1)
{
  assert(C_kplus1.rows() == y_kplus1.cols());
  assert(C_kplus1.cols() == dim);

  if(!initialized){
    Q_k += C_kplus1.transpose()*S_kplus1*C_kplus1;
    Gamma_n += C_kplus1.transpose()*S_kplus1*y_kplus1;

    Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(Q_k);
    if(lu_decomp.rank() == dim){
      P_k = Q_k.inverse();
      x_k = P_k*Gamma_n;
      initialized = true;
    }

    return;
  }
  else{
    Eigen::MatrixXd P_kplus1 = LIN_ALG::matrix_inversion_lemma(P_k, C_kplus1.transpose(), S_kplus1, C_kplus1);
    Eigen::MatrixXd K_kplus1 = P_kplus1 * C_kplus1.transpose() * S_kplus1;

    Eigen::VectorXd x_kplus1;
    if(angle_map == NULL)
      x_kplus1 = x_k + K_kplus1*(y_kplus1 - C_kplus1*x_k);
    else{
     x_kplus1 = x_k + K_kplus1*minimized_angle_vec(y_kplus1 - C_kplus1*x_k,
                                                                      angle_map);
      x_kplus1 = minimized_angle_vec(x_kplus1, angle_map);
    }
 
    //Update Vars
    P_k = P_kplus1;
    x_k = x_kplus1;
  }
}


/*************************************************
 * Weighted Least Squares with Forgetting Factor
 *************************************************/

void MR::EST::WLS_Forget::add_measurement(const Eigen::VectorXd& y_kplus1, 
                                      const Eigen::MatrixXd& S_kplus1, 
                                      const Eigen::MatrixXd& C_kplus1)
{
  assert(C_kplus1.rows() == y_kplus1.cols());
  assert(C_kplus1.cols() == dim);

  //Initialization
  if(!initialized){
    Q_k = lambda*Q_k + C_kplus1.transpose()*S_kplus1*C_kplus1;
    Gamma_n = lambda*Gamma_n + C_kplus1.transpose()*S_kplus1*y_kplus1;

    Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(Q_k);
    if(lu_decomp.rank() == dim){
      P_k = Q_k.inverse();
      x_k = P_k*Gamma_n;
      //      if(angle_map != NULL)
      //  x_k = minimized_angle_vec(x_k, angle_map);
      initialized = true;
    }

    return;
  }
  //Recursion
  else{
    Eigen::MatrixXd P_kplus1 = (1.0/lambda)*LIN_ALG::matrix_inversion_lemma(P_k, 
                                                                            C_kplus1.transpose(), 
                                                                            S_kplus1*(1.0/lambda), 
                                                                            C_kplus1);

    Eigen::MatrixXd K_kplus1 = P_kplus1 * C_kplus1.transpose() * S_kplus1;

    Eigen::VectorXd x_kplus1;
    if(angle_map == NULL)
      x_kplus1 = x_k + K_kplus1*(y_kplus1 - C_kplus1*x_k);
    else{
      x_kplus1 = x_k + K_kplus1*minimized_angle_vec(y_kplus1 - C_kplus1*x_k,
                                                                      angle_map);
      x_kplus1 = minimized_angle_vec(x_kplus1, angle_map);
    }

    //Update Vars
    P_k = P_kplus1;
    //Q_k = Q_kplus1;
    x_k = x_kplus1;
  }
}



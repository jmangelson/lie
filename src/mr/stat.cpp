
#include <iostream>
#include <cmath>

#include <Eigen/Dense>

#include "stat.hpp"
#include "misc.hpp"

Eigen::MatrixXd MR::STAT::rand_cov(int n)
{
  Eigen::MatrixXd L = Eigen::MatrixXd::Random(n,n);
  
  return L.transpose() * Eigen::MatrixXd::Identity(n,n) * L;
}

Eigen::VectorXd MR::STAT::sample_mvg(const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma)
{
  assert(mu.cols() == 1);
  assert(mu.rows() == Sigma.rows());
  assert(Sigma.rows() == Sigma.cols());

  //Generate Gaussian with Variance 1, mu 0
  Eigen::VectorXd samp;
  samp = Eigen::VectorXd::Zero(mu.rows());
  for(int i=0;i<12;i++) {
    samp += Eigen::VectorXd::Random(mu.rows());
  }
  samp *= .5;

  //Calculate Square Root of Covariance
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(Sigma, Eigen::ComputeThinU);
  Eigen::MatrixXd L;
  L = svd.matrixU()*(svd.singularValues().array().sqrt()).matrix().asDiagonal();

  //Generate MV RV with mu and Sigma
  Eigen::VectorXd y(mu.rows());
  y = L*samp + mu;

  return y;
}

Eigen::MatrixXd MR::STAT::sample_mvg(const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma, int N)
{
  assert(mu.cols() == 1);
  assert(mu.rows() == Sigma.rows());
  assert(Sigma.rows() == Sigma.cols());

  //Calculate Square Root of Covariance
  //  Eigen::JacobiSVD<Eigen::MatrixXd> svd(Sigma, Eigen::ComputeThinU);
  Eigen::LLT<Eigen::MatrixXd> llt(Sigma);
  Eigen::MatrixXd L;
  L = llt.matrixL();

  Eigen::MatrixXd Samples(mu.rows(), N);
  for(int i=0;i<N;i++){

    //Generate Gaussian with Variance 1, mu 0
    Eigen::VectorXd samp;
    samp = Eigen::VectorXd::Zero(mu.rows());
    for(int j=0;j<12;j++) {
      samp += Eigen::VectorXd::Random(mu.rows());
    }
    samp *= .5;

    //Calculate Square Root of Covariance
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(Sigma, Eigen::ComputeThinU);
    Eigen::MatrixXd L;
    L = svd.matrixU()*(svd.singularValues().array().sqrt()).matrix().asDiagonal();

    //Generate MV RV with mu and Sigma
    Samples.col(i) = L*samp + mu;
  }

  return Samples;
}

Eigen::VectorXd MR::STAT::mvg_mu(const Eigen::MatrixXd& Samples, const bool angle_map[])
{
  Eigen::VectorXd mu;
  mu = Samples*Eigen::VectorXd::Ones(Samples.cols())*(1.0/Samples.cols());

  if(angle_map != NULL)
    for(int i=0;i<mu.rows();i++)
      if(angle_map[i])
        mu(i) = circular_mean(Samples.row(i).transpose());

  return mu;
}

Eigen::MatrixXd MR::STAT::mvg_cov(const Eigen::MatrixXd& Samples, const bool angle_map[])
{
  Eigen::VectorXd mu = MR::STAT::mvg_mu(Samples, angle_map);
  double N = (double)Samples.cols();

  Eigen::MatrixXd Q;

  if(angle_map == NULL){
    Eigen::MatrixXd tmp = (Samples - mu*Eigen::RowVectorXd::Ones(N));

    Q = tmp*tmp.transpose();
    Q = (Q.array()*(1.0/((double)N-1.0))).matrix();
  
    return Q;  
  }
  else{
    int state_dim = mu.rows();

    Q = Eigen::MatrixXd::Zero(state_dim, state_dim);

    for(int j=0;j<N;j++){
      Eigen::VectorXd diff = Samples.col(j) - mu;
      //std::cout << "Sample " << j << ":\n" << Samples.col(j) << "\n";
      //std::cout << "Diff Pre " << j << ":\n" << diff << "\n";
      for(int i=0;i<state_dim;i++)
        if(angle_map[i])
          diff(i) = minimized_angle(diff(i));
      //std::cout << "Diff " << j << ":\n" << diff << "\n";
      Q += diff*diff.transpose();
    }

    Q *= (1.0/(N - 1.0));
    return Q;
  }
}

Eigen::VectorXd MR::STAT::mrg_stddev(const Eigen::MatrixXd& Sigma, bool angle_map[])
{
  Eigen::VectorXd marg_stddev = Sigma.diagonal().array().sqrt();
  for(int i=0;i<Sigma.rows();i++)
    if(angle_map[i])
      marg_stddev(i) *= 180.0/M_PI;
  return marg_stddev;
}

double MR::STAT::mvg_kld(const Eigen::VectorXd& mu_q, const Eigen::MatrixXd& sigma_q, 
                     const Eigen::VectorXd& mu_p, const Eigen::MatrixXd& sigma_p,
                     const bool angle_map[])
{
  assert(mu_q.rows() == sigma_q.rows()); assert(mu_q.rows() == sigma_q.cols());
  assert(mu_p.rows() == sigma_p.rows()); assert(mu_p.rows() == sigma_p.cols());
  assert(mu_q.rows() == mu_p.rows());

  double k = (double)mu_q.rows();

  Eigen::VectorXd diff = mu_q - mu_p;
  if(angle_map != NULL)
    for(int i=0;i<mu_p.rows();i++)
      if(angle_map[i])
        diff(i) = minimized_angle(diff(i));

  //Eigen::MatrixXd s1_inv = sigma_q.inverse();
  Eigen::LLT<Eigen::MatrixXd> chol_sigma_q(sigma_q);
  double sum = (chol_sigma_q.solve(sigma_p)).trace() + 
    (diff).transpose()*(chol_sigma_q.solve(diff)) -
    k + log(sigma_q.determinant()/sigma_p.determinant());
  /*
  std::cout << "Term1: " << (chol_sigma_q.solve(sigma_p)).trace() << "\n";
  std::cout << "Term2: " << (diff).transpose()*(chol_sigma_q.solve(diff)) << "\n";
  std::cout << "Term3: " << log(sigma_q.determinant()/sigma_p.determinant()) << "\n";
  */
  /*
  std::cout << "Q Det: " << sigma_q.determinant()  << "\n";
  std::cout << "P Det: " << sigma_p.determinant() << "\n";
  std::cout << "Diviser: " << sigma_q.determinant()/sigma_p.determinant() << "\n";
  */

  double kld = sum/2.0;
  
  return kld;  
}

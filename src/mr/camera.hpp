
#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <opencv/cv.h>
#include <opencv2/core/core.hpp>

#include <Eigen/Dense>

#include "mr/ut.hpp"
#include "mr/eigen_util.hpp"

namespace MR
{
  namespace CAMERA
  {

    typedef Eigen::Matrix<double, Eigen::Dynamic, 3> Points3d;


    /**********************
     * Camera Cal         *
     **********************/

    typedef struct _camera_cal_t camera_cal_t;
    struct _camera_cal_t
    {
      Eigen::Matrix3d K;
      Eigen::VectorXd dist_coeffs;
    };

    /****************************
     * 3D-2D Point Reprojection *
     ****************************/

    typedef struct _reprojection_args_t reprojection_args_t;
    struct _reprojection_args_t
    {
      Eigen::MatrixXd K;
      Points3d points3d;
    };

    void setup_reprojection_args(reprojection_args_t *args, Eigen::MatrixXd& points_3d, camera_cal_t* calib);

    Eigen::VectorXd reproject_points(const Eigen::VectorXd& x_cam2tag, void *rp_args);

    /******************************************************
     * Solving for Pose of 3D Objects in the Camera Frame *
     ******************************************************/

    typedef struct _solve_pose_args_t solve_pose_args_t;
    struct _solve_pose_args_t
    {
      //std::vector<cv::Point2f> image_points;
      std::vector<cv::Point3f> object_points;
      cv::Mat cameraMatrix;
      cv::Mat distCoeffs;
    };

    void setup_solve_pose_args(solve_pose_args_t *args, Eigen::MatrixXd& points_3d, camera_cal_t *calib);

    Eigen::VectorXd solve_pose_open_cv(const Eigen::VectorXd& pixel_corners, void *sp_args);

    /*************************************************
     * Finding the Uncertainty of a Pose Measurement *
     *************************************************/
    typedef struct _mc_ret_t mc_return_t;
    struct _mc_ret_t
    {
      Eigen::VectorXd mu_prime;
      Eigen::MatrixXd sigma_prime;
    };

    void mc_uncertainty_characterization(mc_return_t *mc_ret, 
                                         const Eigen::VectorXd& pix_points, 
                                         const Eigen::MatrixXd& Sigma, 
                                         solve_pose_args_t *sp_args, int N=1000);

    Matrix6d covariance_backwards_projection(Eigen::VectorXd& x_cam2tag, 
                                             const Eigen::MatrixXd& Sigma, 
                                             reprojection_args_t *rp_args);

    void run_unscented_trans(MR::UT::ut_return_t *ut_ret, Eigen::VectorXd& pixel_points, 
                             const Eigen::MatrixXd& Sigma, solve_pose_args_t *sp_args, 
                             double alpha=1.0, double kappa=0.0);
  }
}
#endif

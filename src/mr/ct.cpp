
#include <iostream>
#include <cmath>
#include <cassert>

#include "ct.hpp"
#include "diff.hpp"
 
//This code was ported from the Perceptual Robotics Lab perls Repo

Eigen::VectorXd MR::SO3::rot2rph(const Eigen::MatrixXd& R)
{
  Eigen::VectorXd rph(3);
  rph(2) = atan2(R(1,0), R(0,0));

  double sh, ch;
  sh = sin(rph(2));
  ch = cos(rph(2));
    
  rph(1) = atan2(-R(2,0), R(0,0)*ch + R(1,0)*sh);
  rph(0) = atan2(R(0,2)*sh - R(1,2)*ch, -R(0,1)*sh + R(1,1)*ch);

  return rph;
}

Eigen::MatrixXd MR::SO3::rotxyz(double r, double p, double h)
{
  double sr, sp, sh, cr, cp, ch;
  sr = sin(r); cr = cos(r);
  sp = sin(p); cp = cos(p);
  sh = sin(h); ch = cos(h);

  Eigen::MatrixXd R(3,3);
  R <<   
    ch*cp, -sh*cr + ch*sp*sr,  sh*sr + ch*sp*cr,
    sh*cp,  ch*cr + sh*sp*sr, -ch*sr + sh*sp*cr,
    -sp,     cp*sr,             cp*cr;
  return R;
}

Eigen::MatrixXd MR::SO3::homo4x4(const Eigen::VectorXd& X_ij)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6); 

  Eigen::MatrixXd H_ij(4,4);
  H_ij.block(0, 0, 3, 3) = MR::SO3::rotxyz(X_ij(3), X_ij(4), X_ij(5));
  H_ij.block(0, 3, 3, 1) = X_ij.block(0, 0, 3, 1);
  H_ij.block(3, 0, 1, 4) << 0, 0, 0, 1;

  return H_ij;
}

Eigen::VectorXd MR::SSC::inverse(const Eigen::VectorXd& X_ij){
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  return MR::SSC::inverse_fp(X_ij, NULL);
}

Eigen::MatrixXd MR::SSC::inverse_jac(const Eigen::VectorXd& X_ij){
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  return DIFF::num_jacob_central( MR::SSC::inverse_fp, X_ij, NULL);
}

 
Eigen::VectorXd MR::SSC::inverse_fp(const Eigen::VectorXd& X_ij, void * null)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6); 
  
  Eigen::Matrix3d R_ij = MR::SO3::rotxyz(X_ij(3), X_ij(4), X_ij(5));
  
  // X_ji
  const double h_ji = atan2 (R_ij(0,1), R_ij(0,0));
  double sh_ji, ch_ji;
  sh_ji = sin(h_ji);
  ch_ji = cos(h_ji);

  const double x_ji = -(R_ij(0,0)*X_ij(0) + R_ij(1,0)*X_ij(1) + R_ij(2,0)*X_ij(2));
  const double y_ji = -(R_ij(0,1)*X_ij(0) + R_ij(1,1)*X_ij(1) + R_ij(2,1)*X_ij(2));
  const double z_ji = -(R_ij(0,2)*X_ij(0) + R_ij(1,2)*X_ij(1) + R_ij(2,2)*X_ij(2));
  const double r_ji = atan2 (R_ij(2,0)*sh_ji - R_ij(2,1)*ch_ji, -R_ij(1,0)*sh_ji + R_ij(1,1)*ch_ji);
  const double p_ji = atan2 (-R_ij(0,2), R_ij(0,0)*ch_ji + R_ij(0,1)*sh_ji);
  
  Eigen::VectorXd X_ji(6,1);

  X_ji(0) = x_ji;
  X_ji(1) = y_ji;
  X_ji(2) = z_ji;
  X_ji(3) = r_ji;
  X_ji(4) = p_ji;
  X_ji(5) = h_ji;

  return X_ji;
}

Eigen::VectorXd MR::SSC::inverse_SE2(const Eigen::VectorXd& X_ij){
  assert(X_ij.cols()==1); assert(X_ij.rows()==3);
  return MR::SSC::inverse_SE2_fp(X_ij, NULL);
}

Eigen::MatrixXd MR::SSC::inverse_SE2_jac(const Eigen::VectorXd& X_ij){
  assert(X_ij.cols()==1); assert(X_ij.rows()==3);
  return DIFF::num_jacob_central( MR::SSC::inverse_SE2_fp, X_ij, NULL);
}

Eigen::VectorXd MR::SSC::inverse_SE2_fp(const Eigen::VectorXd& X_ij, void * null)
{
   
  assert(X_ij.cols()==1); assert(X_ij.rows()==3);
  Eigen::VectorXd X_ij6 = Eigen::VectorXd(6);
  X_ij6 << X_ij(0), X_ij(1), 0.0, 0.0, 0.0, X_ij(2);
  Eigen::VectorXd X_ji6 = MR::SSC::inverse_fp( X_ij6, NULL);
  Eigen::VectorXd X_ji = Eigen::VectorXd(3);
  X_ji << X_ji6(0), X_ji6(1), X_ji6(5);
  return X_ji;
}

Eigen::VectorXd MR::SSC::head2tail(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_jk.cols()==1); assert(X_jk.rows()==6);
  Eigen::VectorXd x_ij_x_jk(12);
  x_ij_x_jk.head<6>() = X_ij;
  x_ij_x_jk.tail<6>() = X_jk;
  return MR::SSC::head2tail_fp(x_ij_x_jk, NULL);
}

Eigen::MatrixXd MR::SSC::head2tail_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_jk.cols()==1); assert(X_jk.rows()==6);
  Eigen::VectorXd x_ij_x_jk(12);
  x_ij_x_jk.head<6>() = X_ij;
  x_ij_x_jk.tail<6>() = X_jk;
  return DIFF::num_jacob_central(MR::SSC::head2tail_fp, x_ij_x_jk, NULL);
}

Eigen::VectorXd MR::SSC::head2tail_fp(const Eigen::VectorXd& X_ij_X_jk, void * null)
{
  assert(X_ij_X_jk.cols()==1); assert(X_ij_X_jk.rows()==12);

  Vector6d X_ij = X_ij_X_jk.head(6);
  Vector6d X_jk = X_ij_X_jk.tail(6);

  // R_ij, R_jk, R_ik  
  Eigen::Matrix3d R_ij = MR::SO3::rotxyz(X_ij(3), X_ij(4), X_ij(5));
  Eigen::Matrix3d R_jk = MR::SO3::rotxyz(X_jk(3), X_jk(4), X_jk(5));
  Eigen::Matrix3d R_ik;
  R_ik = R_ij*R_jk;

  // X_ij
  const double x_ij = X_ij(0), y_ij = X_ij(1), z_ij = X_ij(2);
  //const double r_ij = X_ij(3), p_ij = X_ij(4), h_ij = X_ij(5);
  
  // X_jk
  const double x_jk = X_jk(0), y_jk = X_jk(1), z_jk = X_jk(2);
  //const double r_jk = X_jk(3), p_jk = X_jk(4); //, h_jk = X_jk(5);

  // X_ik
  const double x_ik = R_ij(0,0)*x_jk + R_ij(0,1)*y_jk + R_ij(0,2)*z_jk + x_ij;
  const double y_ik = R_ij(1,0)*x_jk + R_ij(1,1)*y_jk + R_ij(1,2)*z_jk + y_ij;
  const double z_ik = R_ij(2,0)*x_jk + R_ij(2,1)*y_jk + R_ij(2,2)*z_jk + z_ij;
  const double h_ik = atan2 (R_ik(1,0), R_ik(0,0));
  double sh_ik, ch_ik;
  sh_ik = sin(h_ik);
  ch_ik = cos(h_ik);
  const double r_ik = atan2 (R_ik(0,2)*sh_ik - R_ik(1,2)*ch_ik, -R_ik(0,1)*sh_ik + R_ik(1,1)*ch_ik);
  const double p_ik = atan2 (-R_ik(2,0), R_ik(0,0)*ch_ik + R_ik(1,0)*sh_ik);

  Eigen::VectorXd X_ik(6);

  X_ik(0) = x_ik;
  X_ik(1) = y_ik;
  X_ik(2) = z_ik;
  X_ik(3) = r_ik;
  X_ik(4) = p_ik;
  X_ik(5) = h_ik;

  return X_ik;
}


Eigen::VectorXd MR::SSC::head2tail_SE2(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk)
{
    assert(X_ij.cols()==1); assert(X_ij.rows()==3);
    assert(X_jk.cols()==1); assert(X_jk.rows()==3);
    Eigen::VectorXd x_ij_x_jk(6);
    x_ij_x_jk.head<3>() = X_ij;
    x_ij_x_jk.tail<3>() = X_jk;
    return MR::SSC::head2tail_SE2_fp(x_ij_x_jk, NULL);
}

Eigen::MatrixXd MR::SSC::head2tail_SE2_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_jk)
{
    assert(X_ij.cols()==1); assert(X_ij.rows()==3);
    assert(X_jk.cols()==1); assert(X_jk.rows()==3);
    Eigen::VectorXd x_ij_x_jk(6);
    x_ij_x_jk.head<3>() = X_ij;
    x_ij_x_jk.tail<3>() = X_jk;
    return DIFF::num_jacob_central(MR::SSC::head2tail_SE2_fp, x_ij_x_jk, NULL);
}
 
Eigen::VectorXd MR::SSC::head2tail_SE2_fp(const Eigen::VectorXd& X_ij_X_jk, void * null)
{
    assert(X_ij_X_jk.cols()==1); assert(X_ij_X_jk.rows()==6);
    Eigen::VectorXd X_ij_X_jk12 = Eigen::VectorXd::Zero(12);
    X_ij_X_jk12.head<2>() = X_ij_X_jk.head<2>();
    X_ij_X_jk12(5) = X_ij_X_jk(2);
    X_ij_X_jk12(6) = X_ij_X_jk(3);
    X_ij_X_jk12(7) = X_ij_X_jk(4);
    X_ij_X_jk12(11) = X_ij_X_jk(5);
    
    Eigen::VectorXd X_ik6 = MR::SSC::head2tail_fp(X_ij_X_jk12, NULL);

    Eigen::VectorXd X_ik = Eigen::VectorXd(3);
    X_ik.head<2>() = X_ik6.head<2>();
    X_ik(2) = X_ik6(5);
    return X_ik;
}


Eigen::VectorXd MR::SSC::tail2tail(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_ik.cols()==1); assert(X_ik.rows()==6);
  Eigen::VectorXd x_ij_x_ik(12);
  x_ij_x_ik.head(6) = X_ij;
  x_ij_x_ik.tail(6) = X_ik;
  return MR::SSC::tail2tail_fp(x_ij_x_ik, NULL);
}

Eigen::MatrixXd MR::SSC::tail2tail_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_ik.cols()==1); assert(X_ik.rows()==6);
  Eigen::VectorXd x_ij_x_ik(12);
  x_ij_x_ik.head(6) = X_ij;
  x_ij_x_ik.tail(6) = X_ik;
  return DIFF::num_jacob_central(MR::SSC::tail2tail_fp, x_ij_x_ik, NULL);
}

Eigen::VectorXd MR::SSC::tail2tail_fp(const Eigen::VectorXd& X_ij_X_ik, void * null)
{
  assert(X_ij_X_ik.cols()==1); assert(X_ij_X_ik.rows()==12);

  Vector6d X_ij = X_ij_X_ik.head(6);
  Vector6d X_ik = X_ij_X_ik.tail(6);
  Vector6d X_ji;
  Vector6d X_jk;

  // if(!find_jacob) {
  //Eigen::MatrixXd dummy(1,1);
  X_ji = MR::SSC::inverse(X_ij);
    
  Eigen::VectorXd X_ji_X_ik(12);
  X_ji_X_ik.head(6) = X_ji;
  X_ji_X_ik.tail(6) = X_ik;
  X_jk = MR::SSC::head2tail_fp(X_ji_X_ik, NULL);
  /* }
     else {
     assert(Jtail.cols()==12); assert(Jtail.rows()==6);
     Eigen::MatrixXd Jminus(6,6);
     Eigen::MatrixXd Jplus(6,12);

     SSC::inverse(X_ji, Jminus, X_ij, true);
     SSC::head2tail(X_jk, Jplus, X_ji, X_ik, true);

     //Jacobian
     Jtail.block(0,0,6,6) = Jplus.block(0,0,6,6)*Jminus;
     Jtail.block(0,6,6,6) = Jplus.block(0,6,6,6);
     }*/
  
  return X_jk;
}

Eigen::VectorXd MR::SSC::tail2tail_SE2(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==3);
  assert(X_ik.cols()==1); assert(X_ik.rows()==3);
  Eigen::VectorXd x_ij_x_ik(6);
  x_ij_x_ik.head(3) = X_ij;
  x_ij_x_ik.tail(3) = X_ik;
  return MR::SSC::tail2tail_SE2_fp(x_ij_x_ik, NULL);
}

Eigen::MatrixXd MR::SSC::tail2tail_SE2_jac(const Eigen::VectorXd& X_ij, const Eigen::VectorXd& X_ik)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==3);
  assert(X_ik.cols()==1); assert(X_ik.rows()==3);
  Eigen::VectorXd x_ij_x_ik(6);
  x_ij_x_ik.head(3) = X_ij;
  x_ij_x_ik.tail(3) = X_ik;
  return DIFF::num_jacob_central(MR::SSC::tail2tail_SE2_fp, x_ij_x_ik, NULL);
}

Eigen::VectorXd MR::SSC::tail2tail_SE2_fp(const Eigen::VectorXd& X_ij_X_ik, void * null)
{
    assert(X_ij_X_ik.cols()==1); assert(X_ij_X_ik.rows()==6);
    Eigen::VectorXd X_ij_X_ik12 = Eigen::VectorXd::Zero(12);
    X_ij_X_ik12.head<2>() = X_ij_X_ik.head<2>();
    X_ij_X_ik12(5) = X_ij_X_ik(2);
    X_ij_X_ik12(6) = X_ij_X_ik(3);
    X_ij_X_ik12(7) = X_ij_X_ik(4);
    X_ij_X_ik12(11) = X_ij_X_ik(5);
    
    Eigen::VectorXd X_jk6 = MR::SSC::tail2tail_fp(X_ij_X_ik12, NULL);

    Eigen::VectorXd X_jk = Eigen::VectorXd(3);
    X_jk.head<2>() = X_jk6.head<2>();
    X_jk(2) = X_jk6(5);
    return X_jk;
}


/* Untested
   void
   SO3::quat2rph(const double quat[4], double rph[3])
   {
   rph[0]=atan2(2*(quat[0]*quat[1]+quat[2]*quat[3]),
   1-2*(quat[1]*quat[1]+quat[2]*quat[2]));
   rph[1]=asin(2*(quat[0]*quat[2]-quat[3]*quat[1]));
   rph[2]=atan2(2*(quat[0]*quat[3]+quat[1]*quat[2]),
   1-2*(quat[3]*quat[3]+quat[2]*quat[2]));

   }*/


/*
bool SSC::inverse_jac(Eigen::MatrixXd& X_ji, Eigen::MatrixXd& Jminus, const Eigen::MatrixXd& X_ij, bool find_jacob)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_ji.cols()==1); assert(X_ji.rows()==6);

  Eigen::MatrixXd R_ij = SO3::rotxyz(X_ij(3), X_ij(4), X_ij(5));
  
  // X_ji
  const double h_ji = atan2 (R_ij(0,1), R_ij(0,0));
  double sh_ji, ch_ji;
  sh_ji = sin(h_ji);
  ch_ji = cos(h_ji);

  const double x_ji = -(R_ij(0,0)*X_ij(0) + R_ij(1,0)*X_ij(1) + R_ij(2,0)*X_ij(2));
  const double y_ji = -(R_ij(0,1)*X_ij(0) + R_ij(1,1)*X_ij(1) + R_ij(2,1)*X_ij(2));
  const double z_ji = -(R_ij(0,2)*X_ij(0) + R_ij(1,2)*X_ij(1) + R_ij(2,2)*X_ij(2));
  const double r_ji = atan2 (R_ij(2,0)*sh_ji - R_ij(2,1)*ch_ji, -R_ij(1,0)*sh_ji + R_ij(1,1)*ch_ji);
  const double p_ji = atan2 (-R_ij(0,2), R_ij(0,0)*ch_ji + R_ij(0,1)*sh_ji);
  
  X_ji(0) = x_ji;
  X_ji(1) = y_ji;
  X_ji(2) = z_ji;
  X_ji(3) = r_ji;
  X_ji(4) = p_ji;
  X_ji(5) = h_ji;
  
  if(find_jacob){
    assert(Jminus.cols()==6); assert(Jminus.rows()==6);

    //Find it Numerically
    double del = 0.000001;
    Eigen::MatrixXd in1(6,1);
    Eigen::MatrixXd in2(6,1);
    Eigen::MatrixXd out1(6,1);
    Eigen::MatrixXd out2(6,1);
    Eigen::MatrixXd dummy(1,1);
    for(int i=0;i<6;i++){
      in1 = X_ij;
      in2 = X_ij;
      in1(i) = in1(i)+del;
      in2(i) = in2(i)-del;
     
      inverse(out1, dummy, in1, false);
      inverse(out2, dummy, in2, false);
      
      Jminus.col(i) = (out1 - out2)/(del*2);
    }
  }

  return true;
}

bool SSC::head2tail_jac(Eigen::MatrixXd& X_ik, Eigen::MatrixXd& Jplus, const Eigen::MatrixXd& X_ij, const Eigen::MatrixXd& X_jk, bool find_jacob)
{
  assert(X_ij.cols()==1); assert(X_ij.rows()==6);
  assert(X_jk.cols()==1); assert(X_jk.rows()==6);
  assert(X_ik.cols()==1); assert(X_ik.rows()==6);
  
  // R_ij, R_jk, R_ik  
  Eigen::MatrixXd R_ij = SO3::rotxyz(X_ij(3), X_ij(4), X_ij(5));
  Eigen::MatrixXd R_jk = SO3::rotxyz(X_jk(3), X_jk(4), X_jk(5));
  Eigen::MatrixXd R_ik(3,3);
  R_ik = R_ij*R_jk;

  // X_ij
  const double x_ij = X_ij(0), y_ij = X_ij(1), z_ij = X_ij(2);
  //const double r_ij = X_ij(3), p_ij = X_ij(4), h_ij = X_ij(5);
  
  // X_jk
  const double x_jk = X_jk(0), y_jk = X_jk(1), z_jk = X_jk(2);
  //const double r_jk = X_jk(3), p_jk = X_jk(4); //, h_jk = X_jk(5);

  // X_ik
  const double x_ik = R_ij(0,0)*x_jk + R_ij(0,1)*y_jk + R_ij(0,2)*z_jk + x_ij;
  const double y_ik = R_ij(1,0)*x_jk + R_ij(1,1)*y_jk + R_ij(1,2)*z_jk + y_ij;
  const double z_ik = R_ij(2,0)*x_jk + R_ij(2,1)*y_jk + R_ij(2,2)*z_jk + z_ij;
  const double h_ik = atan2 (R_ik(1,0), R_ik(0,0));
  double sh_ik, ch_ik;
  sh_ik = sin(h_ik);
  ch_ik = cos(h_ik);
  const double r_ik = atan2 (R_ik(0,2)*sh_ik - R_ik(1,2)*ch_ik, -R_ik(0,1)*sh_ik + R_ik(1,1)*ch_ik);
  const double p_ik = atan2 (-R_ik(2,0), R_ik(0,0)*ch_ik + R_ik(1,0)*sh_ik);

  X_ik(0) = x_ik;
  X_ik(1) = y_ik;
  X_ik(2) = z_ik;
  X_ik(3) = r_ik;
  X_ik(4) = p_ik;
  X_ik(5) = h_ik;

  if(find_jacob){
    assert(Jplus.cols()==12); assert(Jplus.rows()==6);

    //Find it Numerically
    double del = 0.000001;
    Eigen::MatrixXd in1(6,1);
    Eigen::MatrixXd in2(6,1);
    Eigen::MatrixXd out1(6,1);
    Eigen::MatrixXd out2(6,1);
    Eigen::MatrixXd dummy(1,1);
    for(int i=0;i<6;i++){
      in1 = X_ij;
      in2 = X_ij;

      in1(i) = in1(i)+del;
      in2(i) = in2(i)-del;
     
      head2tail(out1, dummy, in1, X_jk, false);
      head2tail(out2, dummy, in2, X_jk, false);
      
      Jplus.col(i) = (out1 - out2)/(del*2);
    }
    for(int i=6;i<12;i++){
      in1 = X_jk;
      in2 = X_jk;

      in1(i-6) = in1(i-6)+del;
      in2(i-6) = in2(i-6)-del;
     
      head2tail(out1, dummy, X_ij, in1, false);
      head2tail(out2, dummy, X_ij, in2, false);
      
      Jplus.col(i) = (out1 - out2)/(del*2);
    }
  }

  return true;
}

bool SSC::tail2tail_jac(Eigen::MatrixXd& X_jk, Eigen::MatrixXd& Jtail, const Eigen::MatrixXd& X_ij, const Eigen::MatrixXd& X_ik, bool find_jacob)
{
  Eigen::MatrixXd X_ji(6,1);

  if(!find_jacob) {
    Eigen::MatrixXd dummy(1,1);
    SSC::inverse(X_ji, dummy, X_ij, false);
    SSC::head2tail(X_jk, dummy, X_ji, X_ik, false);
    
  }
  else {
    assert(Jtail.cols()==12); assert(Jtail.rows()==6);
    Eigen::MatrixXd Jminus(6,6);
    Eigen::MatrixXd Jplus(6,12);

    SSC::inverse(X_ji, Jminus, X_ij, true);
    SSC::head2tail(X_jk, Jplus, X_ji, X_ik, true);

    //Jacobian
    Jtail.block(0,0,6,6) = Jplus.block(0,0,6,6)*Jminus;
    Jtail.block(0,6,6,6) = Jplus.block(0,6,6,6);
  }
  
  return true;
}
*/

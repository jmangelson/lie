#include <iostream>
#include "ut.hpp"
#include "misc.hpp"
#include <Eigen/Dense>

void MR::UT::unscented_transform (ut_t *ut, const Eigen::VectorXd& mu, const Eigen::MatrixXd& Sigma,
                          double alpha, double kappa, double beta)
{
  double n = (double)mu.rows();
  double lambda = alpha*alpha*(n + kappa) - n;
  
  Eigen::LLT<Eigen::MatrixXd> llt( Sigma*(n + lambda) );
  Eigen::MatrixXd U = llt.matrixL();
  
  Eigen::VectorXd& mean_weight = ut->mean_weight;
  mean_weight.resize(2*(int)n + 1);
  Eigen::VectorXd& cov_weight = ut->cov_weight;
  cov_weight.resize(2*(int)n + 1);
  Eigen::MatrixXd& sigma_points = ut->sigma_points;
  sigma_points.resize( (int)n, 2*(int)n+1);

  //Setup mean sigma point
  sigma_points.col(0) = mu;
  mean_weight(0) = lambda/(n + lambda);
  cov_weight(0) = (lambda/(n + lambda)) + (1 - alpha*alpha + beta);

  //Setup other sigma points
  for(int i=1;i<=n;i++){
    sigma_points.col(i) = mu + U.col(i-1);
    mean_weight(i) = 1.0/(2.0*(n + lambda));
    cov_weight(i) = 1.0/(2.0*(n + lambda));
  }

  for(int i=n+1;i<(2*n+1);i++){
    sigma_points.col(i) = mu - U.col(i-n-1);
    mean_weight(i) = 1.0/(2.0*(n + lambda));
    cov_weight(i) = 1.0/(2.0*(n + lambda));
  }
  /*
  std::cout << "Sigma Points:\n " << sigma_points << "\n";
  std::cout << "Mean Weight:\n" << mean_weight.transpose() << "\n";
  std::cout << "Cov Weight:\n" << cov_weight.transpose() << "\n";*/

}

void MR::UT::unscented_func (ut_return_t *ut_ret, OutVector (*f)(InVectorRef, void *), ut_t *ut, 
                     void * args, bool angle_map[])
{
  int n = ut->sigma_points.cols();
  
  Eigen::VectorXd y0 = f(ut->sigma_points.col(0), args);
  int m = y0.rows();

  Eigen::MatrixXd y(m,n);
  y.col(0) = y0;

  for(int i=1; i<n; i++)
    y.col(i) = f(ut->sigma_points.col(i), args);

  Eigen::VectorXd& mu_prime = ut_ret->mu_prime;
  mu_prime = (y * ut->mean_weight);
  if (angle_map != NULL){
    for(int i=0;i<m;i++){
      if(angle_map[i]){
        mu_prime(i) = circular_mean(y.row(i).transpose(), ut->mean_weight);
        mu_prime(i) = minimized_angle(mu_prime(i));
      }
    }
  }

  Eigen::MatrixXd& sigma_prime = ut_ret->sigma_prime;
  sigma_prime = Eigen::MatrixXd::Zero(m,m);
  for(int i=0;i<n;i++){
    if(angle_map == NULL){
      sigma_prime += ut->cov_weight(i) * ( (y.col(i) - mu_prime) * 
                                           (y.col(i) - mu_prime).transpose() );
    }
    else{
      sigma_prime += ut->cov_weight(i) * ( minimized_angle_vec (y.col(i) - mu_prime, angle_map) *
                                           minimized_angle_vec (y.col(i) - mu_prime, angle_map).transpose() );
    }
  }

}

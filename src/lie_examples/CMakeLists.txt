file(GLOB example_files *.c *.cpp)
file(GLOB header_files *.h *.hpp)

#set(evaluation_files compose_evaluation_correlated.cpp)
#set(evaluation_files between_evaluation.cpp)

###########################
# Setup Dependencies
set(REQUIRED_PACKAGES lie)


function(add_and_install_execs)
  foreach(source_file ${ARGV})
    message(STATUS "Making ${source_file}")
    get_filename_component(exe_suffix ${source_file} NAME_WE)
    message(STATUS "exe_suffix: ${exe_suffix}")
    string(REGEX REPLACE "_" "-" exe_suffix ${exe_suffix}) # underscore
    set(EXE_NAME ${exe_suffix})
    
    #add_executable(${EXE_NAME} ${source_file} "csv.h")
    add_executable(${EXE_NAME} ${source_file} ${header_files})

    pods_use_pkg_config_packages(${EXE_NAME} ${REQUIRED_PACKAGES})

    pods_install_executables(${EXE_NAME})
  endforeach(source_file)
endfunction()


add_and_install_execs(${example_files})

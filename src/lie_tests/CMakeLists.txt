file(GLOB test_files *tests.c *tests.cpp *tests.cc)

###############################
# SETUP DEPENDENCIES for Tests
set(REQUIRED_PACKAGES lie mr) 

#GMock and Gtest
#set(GMOCK_LIBRARIES gmock gtest gtest_main)
set(GMOCK_LIBRARIES gmock gtest gtest_main)
message(STATUS "GMOCK_LIBRARIES: ${GMOCK_LIBRARIES}")

# Setup test data
# add_subdirectory(test_data)
# set(TEST_DATA_PATH "\"${CMAKE_INSTALL_PREFIX}/test/test_data\"")
# message(STATUS "TEST_DATA_PATH: ${TEST_DATA_PATH}")

set (CMAKE_CXX_FLAGS "-std=c++11 -O3 ${CMAKE_CXX_FLAGS}")

#############################
# Add and Install Tests

function(add_and_install_tests)
  foreach(source_file ${ARGV})
    message(STATUS "Making ${source_file}")
    get_filename_component(exe_suffix ${source_file} NAME_WE)
    message(STATUS "exe_suffix: ${exe_suffix}")
    string(REGEX REPLACE "_" "-" exe_suffix ${exe_suffix}) # underscore
    set(EXE_NAME ${exe_suffix})

    #add_definitions( -DTEST_DATA_PATH=${TEST_DATA_PATH})
    
    #add_executable(${EXE_NAME} ${source_file} "csv.h")
    add_executable(${EXE_NAME} ${source_file})

    pods_use_pkg_config_packages(${EXE_NAME} ${REQUIRED_PACKAGES})

    # Link GMock
    target_link_libraries(${EXE_NAME} ${GMOCK_LIBRARIES})

    add_test(NAME ${EXE_NAME} COMMAND $<TARGET_FILE:${EXE_NAME}>)

    pods_install_tests(${EXE_NAME})
  endforeach(source_file)
endfunction()

add_and_install_tests(${test_files})

